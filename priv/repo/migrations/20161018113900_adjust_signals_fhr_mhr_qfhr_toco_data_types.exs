defmodule OpenTeleCtg.Repo.Migrations.AdjustSignalsFhrMhrQfhrTocoDataTypes do
  use Ecto.Migration

  def change do
    alter table(:ctgs) do
      modify :signals, :text
      modify :fhr, :text
      modify :qfhr, :text
      modify :mhr, :text
      modify :toco, :text
    end
  end

end

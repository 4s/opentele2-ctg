defmodule OpenTeleCtg.Repo.Migrations.MoveSamplesAndSignalsToSubtable do
  use Ecto.Migration

  def change do
    alter table(:ctgs) do
      remove :data
    end

    create table(:samples) do
      add :fhr, :text
      add :qfhr, :text
      add :mhr, :text
      add :toco, :text
      add :ctg_id, references(:ctgs)
      add :sequence_number, :integer

      timestamps
    end

    create table(:signals) do
      add :when, :text
      add :ctg_id, references(:ctgs)

      timestamps
    end

  end
end

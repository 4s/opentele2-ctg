defmodule OpenTeleCtg.Repo.Migrations.ChangeDeviceNameAndStatusTypes do
  use Ecto.Migration

  def change do
    alter table(:ctgs) do
      modify :status, :string
      modify :device_name, :string
    end
  end
end

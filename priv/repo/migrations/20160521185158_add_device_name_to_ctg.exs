defmodule OpenTeleCtg.Repo.Migrations.AddDeviceNameToCtg do
  use Ecto.Migration

  def change do
    alter table(:ctgs) do
      add :device_name, :text
    end
  end
end

defmodule OpenTeleCtg.Repo.Migrations.AddTransferStatusFieldToCtg do
  use Ecto.Migration

  def change do
    alter table(:ctgs) do
      add :status, :text
    end
  end
end

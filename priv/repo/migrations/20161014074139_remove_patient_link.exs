defmodule OpenTeleCtg.Repo.Migrations.RemovePatientLink do
  use Ecto.Migration

  def change do
    alter table(:ctgs) do
      remove :patient_link
    end
  end
end

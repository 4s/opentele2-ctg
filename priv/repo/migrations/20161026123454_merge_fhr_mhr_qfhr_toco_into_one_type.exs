defmodule OpenTeleCtg.Repo.Migrations.MergeFhrMhrQfhrTocoIntoOneType do
  use Ecto.Migration

  def change do
    alter table(:ctgs) do
      add :data, :binary
      remove :fhr
      remove :qfhr
      remove :mhr
      remove :toco
    end
  end

end

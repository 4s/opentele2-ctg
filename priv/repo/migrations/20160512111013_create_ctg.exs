defmodule OpenTeleCtg.Repo.Migrations.CreateCtg do
  use Ecto.Migration

  def change do
    create table(:ctgs) do
      #patient fields
      add :patient_first_name, :string, size: 2048
      add :patient_last_name, :string, size: 2048
      add :patient_external_id, :string, size: 2048
      add :patient_link, :string, size: 2048

      #data fields
      add :external_id, :string, size: 1024
      add :start_time, :string
      add :end_time, :string
      add :ua_delay, :integer
      add :signals, :binary
      add :fhr, :binary
      add :qfhr, :binary
      add :mhr, :binary
      add :toco, :binary

      timestamps
    end

    create index(:ctgs, [:external_id])
  end
end

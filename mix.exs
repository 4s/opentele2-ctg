defmodule OpenTeleCtg.Mixfile do
  use Mix.Project

  def project do
    [
      app: :open_tele_ctg,
      version: "1.1.0",
      elixir: "~> 1.3",
      elixirc_paths: elixirc_paths(Mix.env),
      compilers: [:phoenix] ++ Mix.compilers,
      build_embedded: Mix.env == :prod,
      start_permanent: Mix.env == :prod,
      aliases: aliases,
      deps: deps,
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        "coveralls": :test,
        "coveralls.detail": :test,
        "coveralls.html": :test
      ],
      name: "OpenTeleCTG",
      source_url: "https://bitbucket.org/opentelehealth/opentele-ctg",
      docs: [main: "OpenTeleCTG",
             extras: ["README.md"]]
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [mod: {OpenTeleCtg, []},
     applications: [:phoenix, :cowboy, :logger,
                    :phoenix_ecto, :mariaex, :tds_ecto, :tzdata, :poolboy, :httpoison]]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "web", "test/support"]
  defp elixirc_paths(:jenkins), do: ["lib", "web", "test/support"]
  defp elixirc_paths(_),     do: ["lib", "web"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
     {:bypass, "~> 0.5.1", only: [:test, :jenkins]},
     {:corsica, "~> 0.4"},
     {:cowboy, "~> 1.0"},
     {:credo, "~> 0.3", only: [:dev, :test, :jenkins]},
     {:dialyxir, "~> 0.3", only: [:dev, :test, :jenkins]},
     {:excoveralls, "~> 0.5", only: [:test, :jenkins]},
     {:ex_doc, "~> 0.14", only: :dev},
     {:ex_json_schema, "~> 0.5.1"},
     {:httpoison, "~> 0.9.1"},
     {:junit_formatter, git: "git://github.com/victorolinasc/junit-formatter",
      tag: "v1.1.0", only: [:dev, :test, :jenkins]},
     {:logger_file_backend, "~> 0.0.9"},
     {:mariaex, ">= 0.0.0"},
     {:tds_ecto, "~> 1.0.2"},
     {:phoenix_ecto, "~> 2.0"},
     {:poolboy, "~> 1.5"},
     {:timex, "~> 3.0"},
     {:phoenix, "~> 1.2.1"},
    ]
  end

  # Aliases are shortcut or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      "doc.rest_api": ["opentelectg.gen.api_doc"],
      "doc.dev_api": ["docs"]
    ]
  end
end

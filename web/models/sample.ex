defmodule OpenTeleCtg.Sample do
  @moduledoc ~S"""
  Represents a CTG measurement sample.

  A CTG measurement sample contains:

  - A fhr value, the fetal heart rate, in BPM
  - a qfhr value, the quality of fhr, as a int between 0 and 2,
  - a mhr value, the maternal heart rate, in BPM
  - a toco value, the force of uterine contractions on a scale of 0.0-1.0,
  - a sequence number, the sequence number of the sample, and
  - a reference to th `ctg` object the sample is a part of.
  """

  @type t :: %__MODULE__{fhr: String.t,
                         qfhr: String.t,
                         mhr: String.t,
                         toco: String.t,
                         sequence_number: integer,
                         ctg: Ctg.t}

  require Logger
  use OpenTeleCtg.Web, :model
  alias OpenTeleCtg.Ctg

  schema "samples" do
    field :fhr, :string
    field :qfhr, :string
    field :mhr, :string
    field :toco, :string
    field :sequence_number, :integer
    belongs_to :ctg, Ctg

    timestamps
  end

  @spec add_sample_to_ctg({{number, number, number, number}, number}, Ctg.t)
  :: any
  def add_sample_to_ctg({{fhr, qfhr, mhr, toco}, index}, ctg) do
    Ecto.build_assoc(ctg, :samples,
      %{sequence_number: index,
        fhr: "#{fhr}",
        qfhr: "#{qfhr}",
        mhr: "#{mhr}",
        toco: "#{toco}"
      })
  end

end

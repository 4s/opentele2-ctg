defmodule OpenTeleCtg.Ctg do
  @moduledoc ~S"""
  Represents a complete CTG measurement series.

  A CTG measurement series contains:

  Patient data:

  - The patient's first name,
  - The patient's last name,
  - The patient's external ID, e.g. CPR number in Opentele-server,

  Measurement data:
  - The series' external ID, to be used for fetching its export status
  - The start time of the measurement series,
  - The end time of the measurement series,
  - The device name of the CTG device used for taking the series,
  - The UA delay (or: toco shift) of the measurement series,
  - A list of `Sample`s,
  - A list of `Signal`s, and finally
  - an export status of the measurement series to Milou.

  """

  require Logger
  use OpenTeleCtg.Web, :model
  alias OpenTeleCtg.Repo
  alias OpenTeleCtg.Sample
  alias OpenTeleCtg.Signal

  @type t :: %__MODULE__{patient_first_name: String.t,
                         patient_last_name: String.t,
                         patient_external_id: String.t,
                         external_id: String.t,
                         start_time: String.t,
                         end_time: String.t,
                         device_name: String.t,
                         ua_delay: integer,
                         samples: [Sample.t],
                         signals: [Signal.t],
                         status: String.t}

  schema "ctgs" do
    #patient fields
    field :patient_first_name, :string
    field :patient_last_name, :string
    field :patient_external_id, :string
    # field :patient_link, :string

    #data fields
    field :external_id, :string
    field :start_time, :string
    field :end_time, :string
    field :device_name, :string
    field :ua_delay, :integer

    has_many :samples, Sample
    has_many :signals, Signal

    #status fields
    field :status, :string

    timestamps
  end

  @required_fields ~w(external_id start_time end_time ua_delay
    patient_first_name patient_last_name patient_external_id status device_name)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  @spec from_params(t, map) :: t
  def from_params(model, %{"ctg" => %{"data" => data,
                                      "patientRef" => patient_ref}}) do
    field_values =
    data
    |> from_data_params_to_fields()
    |> Map.merge(from_patient_ref_params_to_fields(patient_ref))
    |> Map.put(:status, Atom.to_string(:initial))

    model
    |> cast(field_values, @required_fields, @optional_fields)
    |> validate_datetime_format(:start_time)
    |> validate_datetime_format(:end_time)

  end

  @spec update_to_exported(t) :: :ok | :error
  def update_to_exported(model) do
    update_status(model, :exported)
  end

  @spec update_to_failed(t) :: :ok | :error
  def update_to_failed(model) do
    update_status(model, :failed)
  end

  @spec pending_export :: [t]
  def pending_export do
    OpenTeleCtg.Ctg
    |> where([c], c.status == "initial")
    |> Repo.all()
  end

  @spec load_associations(t) :: [t]
  def load_associations(ctg) do
    Repo.preload ctg, [:signals, :samples]
  end

  defp update_status(model, status) do
    model = Ecto.Changeset.change model, status: Atom.to_string(status)

    case Repo.update model do
      {:ok, _} -> :ok
      {:error, _} -> :error
    end
  end

  defp validate_datetime_format(changeset, :signals) do
    values =
    changeset
    |> get_field(:signals)
    |> String.split(",")
    |> Enum.filter(&(String.length(&1) > 0))
    |> Enum.reduce(changeset, &(validate_datetime_format(&2, :signals, &1)))
  end

  defp validate_datetime_format(changeset, field) when is_atom(field) do
    value = get_field(changeset, field)
    validate_datetime_format(changeset, field, value)
  end

  defp validate_datetime_format(changeset, field, value) do
    case Timex.parse(value, "{ISO:Extended:Z}") do
      {:ok, _} ->
        changeset
      {:error, term} ->
        error_msg = "#{inspect term}"
        changeset |> add_error(field, error_msg)
    end
  end

  defp from_patient_ref_params_to_fields(patient_ref) do
    %{
      patient_first_name:   Map.get(patient_ref, "firstName"),
      patient_last_name:    Map.get(patient_ref, "lastName"),
      patient_external_id:  Map.get(patient_ref, "uniqueId")
      # patient_link:         get_in(patient_ref, ["links", "patient"])
    }
  end

  defp from_data_params_to_fields(data) do
    %{
      external_id:  Map.get(data, "reference"),
      start_time:   Map.get(data, "startTime"),
      end_time:     Map.get(data, "endTime"),
      device_name:    Map.get(data, "deviceName"),
      ua_delay:   Map.get(data, "uaDelay"),
      signals:      data |> Map.get("signals") |> Enum.join(","),
    }
  end
end

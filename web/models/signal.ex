defmodule OpenTeleCtg.Signal do
  @moduledoc ~S"""
  Represents a CTG signal.

  A CTG signal contains:

  - A timestamp of when the signal was fired.

  """

  @type t :: %__MODULE__{when: String.t,
                         ctg: Ctg.t}

  require Logger
  use OpenTeleCtg.Web, :model
  alias OpenTeleCtg.Ctg

  schema "signals" do
    field :when, :string
    belongs_to :ctg, Ctg

    timestamps
  end

  @spec add_signal_to(t, Ctg.t) :: any
  def add_signal_to(signal, ctg) do
    Ecto.build_assoc(ctg, :signals, when: signal)
  end

end

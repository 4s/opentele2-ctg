defmodule OpenTeleCtg.CtgStatusController do
  @moduledoc ~S"""
  Controller for fetching the status of one or more CTG measurement export jobs.
  """

  use OpenTeleCtg.Web, :controller
  alias OpenTeleCtg.Ctg
  require Logger

  plug :scrub_params, "reference" when action in [:status]

  @doc ~S"""
  Given one or more references to already posted CTG measurement series, returns
  a list of export statuses for these same CTG measurement series.
  """
  @spec status(Conn.t, map | list) :: Conn.t
  def status(conn, %{"reference" => references}) when is_list(references) do
    ctgs =
      Ctg
      |> where([ctg], ctg.external_id in ^references)
      |> Repo.all()

    case ctgs do
      [] ->
        conn
        |> put_status(404)
        |> render(OpenTeleCtg.ErrorView, "404.json")

      ctgs ->
        conn
        |> render("status.json", ctgs: ctgs)
    end
  end

  def status(conn, %{"reference" => reference}) do
    status(conn, %{"reference" => [reference]})
  end

end

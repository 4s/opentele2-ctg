defmodule OpenTeleCtg.IsAliveController do
  @moduledoc ~S"""
  Controller for checking the `is alive` status of the opentele-ctg service.
  """

  use OpenTeleCtg.Web, :controller
  alias OpenTeleCtg.{ApiError, Responses}

  @doc ~S"""
  Returns the status of the opentele-ctg service.
  """
  @spec get(Conn.t, map) :: Conn.t
  def get(conn, _) do
    case Ecto.Adapters.SQL.query(OpenTeleCtg.Repo, "SELECT 1", []) do
      {:ok, _} ->
        conn
        |> render("is_alive.json")

      {:error, _exception} ->
        conn
        |> Responses.internal_server_error([build_error])
    end
  end

  @spec build_error :: %ApiError{}
  defp build_error do
    %ApiError{resource: "is_alive",
              field: "",
              code: "Could not reach database."}
  end

end

defmodule OpenTeleCtg.RootController do
  @moduledoc ~S"""
  Controller for showing the available resources on the service.
  """

  use OpenTeleCtg.Web, :controller

  @doc ~S"""
  Returns a JSON description of the resources available on the
  opentele-ctg service.
  """
  @spec get(Conn.t, map) :: Conn.t
  def get(conn, _) do
    root_info = %{
      server_environment: Mix.env(),
      api_version: OpenTeleCtg.Mixfile.project[:version],
      schemas: schemas_url(conn, :schemas),
      api_doc: static_url(conn, "/docs/ctg-api.html"),
      self: root_url(conn, :get),
      continuous_ctg: continuous_ctg_url(conn, :create),
      continuous_ctg_status: continuous_ctg_status_url(conn, :status, ""),
      ctg: ctg_url(conn, :create),
      ctg_status: ctg_status_url(conn, :status),
      is_alive: is_alive_url(conn, :get)
    }

    render(conn, "get.json", root_info)
  end
end

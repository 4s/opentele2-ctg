defmodule OpenTeleCtg.SchemasController do
  @moduledoc ~S"""
  Controller for listing the JSON schema associated with the service's
  resources.
  """

  use OpenTeleCtg.Web, :controller

  @doc ~S"""
  Renders the list of JSON schemas of the service's resources.
  """
  @spec schemas(Conn.t, map) :: Conn.t
  def schemas(conn, _params) do

    schema_definitions = [
      create_ctg_schema: static_url(conn,
        "/json_schemas/create-ctg.json"),

      create_continuous_ctg_schema: static_url(conn,
        "/json_schemas/create-continuous-ctg.json"),

      patient_schema: static_url(conn,
        "/json_schemas/patient-definitions.json")
    ]

    render conn, schema_definitions
  end

end

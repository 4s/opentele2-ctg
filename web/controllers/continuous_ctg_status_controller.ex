defmodule OpenTeleCtg.ContinuousCtgStatusController do
  @moduledoc ~S"""
  Controller for fetching the status of any ongoing continuous CTG measurement
  series for a given patient.
  """

  use OpenTeleCtg.Web, :controller
  alias OpenTeleCtg.Milou.Continuous.StatusRegistry
  require Logger

  @doc ~S"""
  Fetches the continuous CTG measurement status of the
  specified `patient_unique_id`.
  """
  @spec status(Conn.t, map) :: Conn.t
  def status(conn, %{"patient_unique_id" => patient_unique_id})  do
    status =
      %OpenTeleCtg.PatientReference{unique_id: patient_unique_id}
      |> StatusRegistry.poll_status()

    conn
    |> put_status(200)
    |> render("status.json", status: status)
  end

end

defmodule OpenTeleCtg.ContinuousCtgController do
  @moduledoc ~S"""
  Controller for receiving continuous CTG measurement events.
  """

  require Logger
  use OpenTeleCtg.Web, :controller
  alias OpenTeleCtg.{CtgStart, CtgStop, CtgSample, CtgSignal,
                     PatientReference, Responses, ApiError}
  alias OpenTeleCtg.Milou.Continuous.StatusRegistry

  @exporter Application.get_env(:open_tele_ctg,
    OpenTeleCtg.Milou)[:continuous_exporter] || OpenTeleCtg.Milou.Continuous

  plug :authorize, [permissions: ["ROLE_REALTIME_CTG_SAVE"]] when action in [:create]
  plug :validate when action in [:create]
  plug :scrub_params, "continuousCtg" when action in [:create]

  @type maybe_result :: {:ok, list} | {:error, any}

  @doc ~S"""
  Given a set of parameters containing a `continuousCtg` object which contains
  a `patientRef` and event `data`, parse the different types of CTG events in
  `data` and send to the continuous CTG exporter associated with the given
  `patientRef`.
  """
  @spec create(Conn.t, map) :: Conn.t
  def create(conn, %{"continuousCtg" => %{"patientRef" => patient_reference,
                                          "data" => events}}) do

    Logger.debug("Events: #{inspect events, char_lists: false}")
    patient_reference = PatientReference.from_params(patient_reference)

    exporter_result =
      events
      |> Enum.map(&parse_event/1)
      |> add_status_context
      |> handle_start_event(patient_reference)
      |> handle_sample_and_signal_events(patient_reference)
      |> handle_stop_event(patient_reference)

    case exporter_result do
      {:ok, _} ->
        conn
        |> put_status(201)
        |> render("created.json")

      {:error, reason} ->
        Logger.debug "Error trying to process events: #{inspect reason, char_lists: false}"
        api_error = build_error reason

        conn
        |> Responses.internal_server_error([api_error])
    end
  end

  defp parse_event(%{"ctgStart" => ctg_start}), do: CtgStart.from_params(ctg_start)
  defp parse_event(%{"ctgStop" => ctg_stop}), do: CtgStop.from_params(ctg_stop)
  defp parse_event(%{"ctgSample" => ctg_sample}), do: CtgSample.from_params(ctg_sample)
  defp parse_event(%{"ctgSignal" => ctg_signal}), do: CtgSignal.from_params(ctg_signal)

  @spec add_status_context(any) :: {:ok, any}
  defp add_status_context(whatever), do: {:ok, whatever}

  @spec handle_start_event(maybe_result, PatientReference.t) :: maybe_result
  defp handle_start_event(ctg_events, patient_reference) do
    is_start = fn ctg_event -> ctg_event.__struct__ == CtgStart end

    export_start = fn (patient_reference, ctg_start_events) ->
      @exporter.start(patient_reference, List.first(ctg_start_events))
      StatusRegistry.register(patient_reference,
        List.first(ctg_start_events).ua_sensitivity)
    end

    handle_events(ctg_events, patient_reference, is_start, export_start)
  end

  @spec handle_sample_and_signal_events(maybe_result, PatientReference.t) :: maybe_result
  defp handle_sample_and_signal_events(ctg_events, patient_reference) do
    is_sample_or_signal = fn ctg_event ->
      ctg_event.__struct__ == CtgSample || ctg_event.__struct__ == CtgSignal
    end

    export_samples_and_signals = fn (patient_reference, ctg_events) ->

      StatusRegistry.refresh(patient_reference)
      @exporter.export(patient_reference, ctg_events)

    end
    handle_events(ctg_events, patient_reference, is_sample_or_signal, export_samples_and_signals)
  end

  @spec handle_stop_event(maybe_result, PatientReference.t) :: maybe_result
  defp handle_stop_event(ctg_events, patient_reference) do
    is_stop = fn ctg_event -> ctg_event.__struct__ == CtgStop end
    export_stop = fn (patient_reference, ctg_stop_events) ->
      @exporter.stop(patient_reference, List.first(ctg_stop_events))

      StatusRegistry.unregister(patient_reference)
    end
    handle_events(ctg_events, patient_reference, is_stop, export_stop)
  end

  @spec handle_events(maybe_result, PatientReference.t, fun, fun)
  :: maybe_result
  defp handle_events({:error, reason}, _, _, _), do: {:error, reason}
  defp handle_events({:ok, events},
    patient_reference, predicate_fun, export_fun) do

    {ctg_events, rest} =
      events
      |> Enum.partition(predicate_fun)

    if not Enum.empty? ctg_events do
      result = export_fun.(patient_reference, ctg_events)

      case result do
        :ok -> {:ok, rest}
        {:error, reason} -> {:error, reason}
      end

    else
      {:ok, rest}
    end
  end

  @spec build_error(atom) :: ApiError.t
  defp build_error(:not_found) do
    ApiError.new("continuous_ctg",
      "continuous_ctg.data",
      "Could not find exporter for patient. " <>
        "Either no CtgStart event has been sent or " <>
        " the exporter has timed out.")
  end

  @spec build_error(atom) :: ApiError.t
  defp build_error(:could_not_create) do
    ApiError.new("continuous_ctg",
      "continuous_ctg.data",
      "Could not create CTG exporter")
  end

end

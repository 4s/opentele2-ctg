defmodule OpenTeleCtg.CtgController do
  @moduledoc ~S"""
  Controller for receiving whole CTG measurement series, which are then batch
  processed and send to Milou by a background job.
  """

  use OpenTeleCtg.Web, :controller
  alias OpenTeleCtg.{Ctg, Signal, Sample}
  require Logger

  @exporter Application.get_env(:open_tele_ctg,
    OpenTeleCtg.Milou)[:discrete_exporter] || OpenTeleCtg.Milou.Discrete

  plug :validate when action in [:create]
  plug :scrub_params, "ctg" when action in [:create]

  @doc """
  Given a set of parameters containing a `ctg` object, transform the `ctg` data
  into a `Ctg` struct and save to database.
  """
  @spec create(Conn.t, map) :: Conn.t
  def create(conn, params) do
    changeset = Ctg.from_params(%Ctg{}, params)

    Logger.debug("Changeset: #{inspect changeset, char_lists: false}")

    transaction_result = Repo.transaction fn ->
      ctg = Repo.insert!(changeset)

      #Add signals
      params["ctg"]["data"]["signals"]
      |> Enum.each(fn(signal) ->
        Repo.insert!(Signal.add_signal_to(signal, ctg))
      end)

      #Add samples
      fhr  = params["ctg"]["data"]["fhr"]
      qfhr = params["ctg"]["data"]["qfhr"]
      mhr  = params["ctg"]["data"]["mhr"]
      toco = params["ctg"]["data"]["toco"]

      List.zip([fhr, qfhr, mhr, toco])
      |> Stream.with_index()
      |> Stream.each(fn(sample_and_index) ->
        Repo.insert!(Sample.add_sample_to_ctg(sample_and_index, ctg))
      end)
      |> Stream.run()

      ctg
    end

    case transaction_result do
      {:ok, ctg} ->
        trigger_export(ctg)

        conn
        |> put_status(201)
        |> render("created.json", ctg: ctg)

      {:error, changeset} ->
        Logger.warn("Could not save ctg: #{printable_errors(changeset)}")

        conn
        |> put_status(500)
        |> render(OpenTeleCtg.ErrorView, "500.json", error: changeset.errors)
      end
  end

  defp trigger_export(ctg) do
    @exporter.export(ctg: ctg)
  end

  defp printable_errors(%Ecto.Changeset{errors: errors}) do
    errors
    |> Enum.reduce("", fn(error, acc) -> acc <>
        "Field: '#{elem(error, 0)}'  Error: '#{elem(error, 1)}' \n"
    end)
  end

end

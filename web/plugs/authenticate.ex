defmodule OpenTeleCtg.Authenticate do
  @moduledoc ~S"""
  Authenticates a requester by checking their credentials against
  registered users.

  Works as a `Plug` and extracts the `authorization` headers of each incoming
  connection (`Conn`) object and checks that the user exists in the database.
  """

  require Logger
  import Plug.Conn
  alias OpenTeleCtg.OpenTeleServer.User

  @repo Application.get_env(:open_tele_ctg,
    OpenTeleCtg.OpenTeleServer)[:repo] || OpenTeleCtg.OpenTeleServer.Repo
  @api_key Application.get_env(:open_tele_ctg, :api_key)

  @doc ~S"""
  Initializes the authentication plug.
  """
  @spec init(any) :: nil
  def init(_opts) do
    nil
  end

  @doc ~S"""
  Authenticates an incoming connection based on its `authorization` header.
  """
  @spec call(Conn.t, Keyword.t) :: Conn.t
  def call(conn, _opts) do
    Logger.debug("Auth plug called")

    result =
      conn
      |> get_req_header("authorization")
      |> extract_credentials

    Logger.debug("Result: #{inspect result}")

    case result do
      {:ok, username, hashed_password} ->
        Logger.debug("Looking up user: #{username} and pass: #{hashed_password}")
        case authenticate(username, hashed_password) do
          :ok ->
            conn
            |> assign(:authenticated, true)
            |> assign(:username, username)

          {:error, _} ->
            conn
        end

      {:ok, api_key} ->
        Logger.debug("Looking up API key: #{inspect api_key}")
        case authenticate(api_key) do
          :ok ->
            conn
            |> assign(:authenticated, true)
            |> assign(:username, "opentele-server.CtgExportJob")

          {:error, _} ->
            conn
        end

      {:error, _} ->
        conn
    end
  end

  @spec extract_credentials([String.t]) :: {:ok, String.t}
  defp extract_credentials(["API key " <> api_key]) do
    api_key
    |> Base.decode64([])
  end

  @spec extract_credentials([String.t]) :: {:ok, String.t, String.t}
  defp extract_credentials(["Basic " <> encoded_string]) do
    encoded_string
    |> Base.decode64([])
    |> hash_password()
  end

  @spec extract_credentials(any) :: {:error, String.t}
  defp extract_credentials(header) do
    {:error, "Unknown type of auth header found in request: #{inspect header}"}
  end

  @spec hash_password(
    {:ok, String.t} | {:error, String.t} | :error
  ) :: {:ok, String.t, String.t} | {:error, String.t} | :error
  defp hash_password({:ok, credentials}) do
    hashed =
      credentials
      |> String.split(":")
      |> List.last
      |> hash_sha256
      |> Base.encode16

    username =
      credentials
      |> String.split(":")
      |> List.first

    {:ok, username, hashed}
  end

  defp hash_password({:error, reason} = error) do
    Logger.error("Unable to hash password: #{reason}")
    error
  end

  defp hash_password(:error) do
    reason = "Unable to base64 decode the username/pw string"
    Logger.error(reason)
    {:error, reason}
  end

  @spec hash_sha256(String.t) :: String.t
  defp hash_sha256(password) do
    :crypto.hash(:sha256, password)
  end

  @spec authenticate(String.t) :: any
  defp authenticate(api_key) do
    #Logger.debug("Known API KEY: #{@api_key}")
    #Logger.debug("Received API KEY: #{api_key}")

    if api_key == @api_key do
      :ok
    else
      {:error, "Unknown API key"}
    end
  end

  @spec authenticate(String.t, String.t) :: any
  defp authenticate(username, hashed_password) do
    case @repo.get_by(User, username: username, password: hashed_password) do
      nil -> {:error, "Could not find user"}
      _ -> :ok
    end
  end

end

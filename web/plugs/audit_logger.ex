defmodule OpenTeleCtg.AuditLogger do
  @moduledoc """
  Logs all actions to disk for traceability.

  Works as a `Plug` and writes audit log information of each incoming
  connection (`Conn`) object to disk.
  """

  require Logger
  import Plug.Conn
  alias Plug.Conn

  @repo Application.get_env(:open_tele_ctg,
    OpenTeleCtg.OpenTeleServer)[:repo] || OpenTeleCtg.OpenTeleServer.Repo

  @doc ~S"""
  Initializes the audit logger plug.
  """
  @spec init(any) :: nil
  def init(_opts) do
    nil
  end

  @doc ~S"""
  Writes audit information about the specified `Conn` to a log on disk.
  """
  @spec call(Conn.t, Keyword.t) :: Conn.t
  def call(conn, _opts) do
    register_before_send(conn, fn conn ->
      write_log_entry(conn)
      conn
    end)
  end

  @spec write_log_entry(Conn.t) :: :ok
  defp write_log_entry(conn) do
    Logger.debug "Conn: #{inspect conn}"

    username = Map.get(conn.assigns, :username)
    timestamp = Timex.format!(Timex.now, "{ISO:Extended:Z}")
    client_ip = get_client_ip conn

    Logger.info("type=audit " <>
      "ip=#{client_ip} " <>
      "username=#{username} " <>
      "timestamp=#{timestamp} " <>
      "action=\"#{conn.method} #{conn.request_path}\" " <>
      "http_status=#{conn.status}", audit: true)
  end

  @spec get_client_ip(Conn.t) :: binary
  defp get_client_ip(conn) do
    remote_ips = get_req_header(conn, "x-forwarded-for")
    case List.first(remote_ips) do
      nil -> Enum.join(Tuple.to_list(conn.remote_ip), ".")
      ip -> ip
    end
  end

end

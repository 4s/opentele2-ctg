defmodule OpenTeleCtg.EnsureAuthenticated do
  @moduledoc ~S"""
  Ensures that a user is authenticated.
  """

  require Logger
  import Plug.Conn

  @doc ~S"""
  Initializes the ensure authenticated plug.
  """
  @spec init(any) :: nil
  def init(_opts) do
    nil
  end

  @doc ~S"""
  Ensures that the incoming connection is authenticated.
  """
  @spec call(Conn.t, Keyword.t) :: Conn.t
  def call(conn, _opts) do
    if conn.assigns[:authenticated] == nil ||
      conn.assigns[:authenticated] == false do
      Logger.info("User not authenticated")
      conn
      |> send_resp(401, "")
      |> halt
    else
      Logger.debug("User authenticated")
      conn
    end
  end

end

defmodule OpenTeleCtg.ValidateSchema do
  @moduledoc ~S"""
  Grabs an incoming request if it has a JSON body and sends it to a JSON schema
  validator.
  """

  require Logger
  import Plug.Conn

  alias OpenTeleCtg.JSONSchema.Validator
  alias OpenTeleCtg.Responses

  @spec validate(Conn.t, Keyword.t) :: Conn.t
  def validate(conn, _opts) do

    Logger.debug("JSON schema plug called")
    path = List.first(conn.path_info)
    data = conn.params

    case Validator.validate(%{data: data, path: path}) do
      :ok -> conn
      {:error, errors} ->
        Logger.error "Errors received from validate: #{inspect errors}"
        conn
        |> Responses.unprocessable_entity(errors)
        |> halt
    end
  end

end

defmodule OpenTeleCtg.Authorize do
  @moduledoc ~S"""
  Authorizes a requester by checking for relevant permissions.

  Works as a `Plug` and extracts the `authorization` headers of each incoming
  connection (`Conn`) object and checks that the user exists in the database.
  """

  require Logger
  import Plug.Conn
  import Ecto.Query
  alias OpenTeleCtg.OpenTeleServer.{User, UserRole, RolePermission, Permission}

  @repo Application.get_env(:open_tele_ctg, OpenTeleCtg.OpenTeleServer)[:repo] || OpenTeleCtg.OpenTeleServer.Repo

  @doc ~S"""
  Initializes the authorization plug.
  """
  @spec init(any) :: nil
  def init(_opts) do
    nil
  end

  @doc ~S"""
  Authorizes an incoming connection based on the permissions associated
  with the authenticated user of the connection.
  """
  @spec authorize(Conn.t, Keyword.t) :: Conn.t
  def authorize(conn, opts) do
    permissions = Keyword.fetch!(opts, :permissions)
    username = Map.get(conn.assigns, :username)
    user_permissions = lookup_permissions(username)

    authorized =
      permissions
      |> Enum.any?(fn permission ->
      Enum.member?(user_permissions, permission)
    end)

    if authorized do
      Logger.debug("Request '#{conn.request_path}' authorized for user '#{username}'")
      conn

    else
      Logger.info("Request '#{conn.request_path}' not authorized for user '#{username}'. Permissions needed: '#{inspect permissions}'")
      conn
      |> send_resp(403, "")
      |> halt
    end
  end

  @doc ~S"""
  Looks up the permissions of the specified username.
  """
  @spec lookup_permissions(String.t | nil) :: [Permission.t]
  def lookup_permissions(nil) do
    Logger.debug "Username was undefined when trying to lookup permissions"
    []
  end

  def lookup_permissions(username) do

    with user <- @repo.get_by(User, username: username),
         user_role_ids <- @repo.all(user_roles_by_user_id(user)),
         permission_ids <- @repo.all(permission_ids_by_user_role(user_role_ids)),
         permissions <- @repo.all(permission_names(permission_ids)) do

      Logger.debug("Found permissions: '#{inspect permissions}'")
      permissions

    else
      error ->
        Logger.debug("Error occurred: '#{inspect error}', trying to lookup" <>
          " user permissions for username: '#{username}'")
        []
    end
  end

  @spec user_roles_by_user_id(User.t) :: [UserRole.t]
  defp user_roles_by_user_id(user) do
    from user_role in UserRole,
      where: user_role.user_id == ^user.id,
      select: user_role.role_id
  end

  @spec permission_ids_by_user_role([String.t]) :: [RolePermission.t]
  defp permission_ids_by_user_role(user_role_ids) do
    from role_permission in RolePermission,
      where: role_permission.role_id in ^user_role_ids,
      select: role_permission.permission_id
  end

  @spec permission_names([String.t]) :: [Permission.t]
  defp permission_names(permission_ids) do
    from permission in Permission,
      where: permission.id in ^permission_ids,
      select: permission.permission
  end

end

defmodule OpenTeleCtg.Router do
  @moduledoc ~S"""
  Module in charge of setting up the routes of the REST API.
  """

  use OpenTeleCtg.Web, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :protected do
    plug OpenTeleCtg.EnsureAuthenticated
  end

  scope "/", OpenTeleCtg do
    pipe_through :api

    get "/is_alive", IsAliveController, :get
    get "/", RootController, :get
    get "/schemas", SchemasController, :schemas

    scope "/ctg" do
      pipe_through :protected
      post "/", CtgController, :create
      get "/status", CtgStatusController, :status
    end

    scope "/continuous_ctg" do
      pipe_through :protected
      post "/", ContinuousCtgController, :create
      get "/status/:patient_unique_id", ContinuousCtgStatusController, :status
    end

  end
end

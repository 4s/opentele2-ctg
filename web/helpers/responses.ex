defmodule OpenTeleCtg.Responses do
  @moduledoc """
  Contains helper functions for HTTP responses.
  """

  use OpenTeleCtg.Web, :controller
  require Logger

  alias Plug.Conn
  alias OpenTeleCtg.{ErrorView, ErrorBundle, ApiError}

  @doc """
  Error response when a resource cannot be found.
  """
  @spec resource_not_found(Conn.t, String.t, String.t) :: Conn.t
  def resource_not_found(conn, resource_name, id) do
    Logger.info "The following resource '#{resource_name}' with id: '#{id}' was requested but not found"

    conn
    |> put_status(:not_found)
    |> render(ErrorView, "404.json", %{resource: resource_name})
  end

  @doc """
  Error response for an internal server error.
  """
  @spec internal_server_error(Conn.t, [%ApiError{}]) :: Conn.t
  def internal_server_error(conn, errors) do
    Logger.error "The following error(s) occurred: #{inspect errors}"
    error_bundle = %ErrorBundle{message: "Internal server error", errors: errors}

    conn
    |> put_status(:internal_server_error)
    |> render(ErrorView, "500.json", error_bundle)
  end

  @doc """
  Error response when the received JSON does not conform to the specified schema.
  """
  @spec unprocessable_entity(Conn.t, [%ApiError{}]) :: Conn.t
  def unprocessable_entity(conn, errors) do
    Logger.debug "Errors: #{inspect errors}"
    error_bundle = %ErrorBundle{message: "Unprocessable entity", errors: errors}

    conn
    |> put_status(:unprocessable_entity)
    |> render(ErrorView, "422.json", error_bundle)
  end

end

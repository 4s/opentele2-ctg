defmodule OpenTeleCtg.SchemasView do
  @moduledoc ~S"""
  Module for rendering the 'schemas' view.
  """

  use OpenTeleCtg.Web, :view

  @doc ~S"""
  Renders the schemas view.
  """
  @spec render(String.t, map) :: map
  def render("schemas.json",
    %{create_ctg_schema: ctg_schema,
      create_continuous_ctg_schema: continuous_ctg_schema,
      patient_schema: patient_schema}) do

    %{
      "self": schemas_url(OpenTeleCtg.Endpoint, :schemas),
      "create-ctg": ctg_schema,
      "create-continuous-ctg": continuous_ctg_schema,
      "patient-definitions": patient_schema
    }
  end

end

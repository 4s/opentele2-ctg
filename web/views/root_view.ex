defmodule OpenTeleCtg.RootView do
  @moduledoc ~S"""
  Module for rendering the 'root' view.
  """

  use OpenTeleCtg.Web, :view

  @doc ~S"""
  Renders the root resource view.
  """
  @spec render(String.t, map) :: map
  def render("get.json", info) do
    %{
      apiVersion: info[:api_version],
      serverEnvironment: info[:server_environment],
      links: %{
        self: info[:self],
        apiDoc: info[:api_doc],
        schemas: info[:schemas],
        continuousCtg: info[:continuous_ctg],
        continuousCtgStatus: info[:continuous_ctg_status],
        ctg: info[:ctg],
        ctgStatus: info[:ctg_status],
        isAlive: info[:is_alive]
      }
    }
  end
end

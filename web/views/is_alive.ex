defmodule OpenTeleCtg.IsAliveView do
  @moduledoc ~S"""
  Module for rendering the 'is alive' view.
  """

  use OpenTeleCtg.Web, :view

  @doc """
  Renders the 'is alive' view.
  """
  @spec render(String.t, map) :: map
  def render("is_alive.json", _map) do
    %{status: "ok"}
  end

end

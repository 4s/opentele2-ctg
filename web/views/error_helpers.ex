defmodule OpenTeleCtg.ErrorHelpers do
  @moduledoc ~S"""
  Module for building error views.
  """

  alias OpenTeleCtg.{ApiError, ErrorBundle}

  @doc ~S"""
  Builds a resource not found error view.
  """
  @spec build_resource_not_found(String.t) :: %ErrorBundle{}
  def build_resource_not_found(resource_name) do
    %ErrorBundle{message: "Not found",
                 errors: [%ApiError{resource: resource_name,
                                    field: "id",
                                    code: "not found"}]}
  end

end

defmodule OpenTeleCtg.ContinuousCtgView do
  @moduledoc ~S"""
  View for rendering the create continuous CTG response.
  """

  use OpenTeleCtg.Web, :view

  @doc ~S"""
  Renders the continuous CTG created response.
  """
  @spec render(String.t, map) :: map
  def render("created.json", _params) do
    %{status: "ok"}
  end

end

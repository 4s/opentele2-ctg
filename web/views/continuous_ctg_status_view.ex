defmodule OpenTeleCtg.ContinuousCtgStatusView do
  @moduledoc ~S"""
  View for rendering continuous CTG status responses.
  """

  use OpenTeleCtg.Web, :view

  @doc ~S"""
  Renders the continuous CTG status view.
  """
  @spec render(String.t,
    %{status: {:ok, :none}} |
    %{status: {:ok, :timeout, binary}} |
    %{status: {:ok, binary, binary}}
  ) :: map
  def render("status.json", %{status: {:ok, :none}}), do: %{status: :none}

  def render("status.json", %{status: {:ok, :timeout, timestamp}}) do
    %{
      status: :timeout,
      last_event: timestamp |> Timex.format("{ISO:Extended:Z}") |> elem(1)
    }
  end

  def render("status.json", %{status: {:ok, selected_sensitvity, timestamp}}) do
    %{
      status: :active,
      sensitivity: selected_sensitvity,
      last_event: timestamp |> Timex.format("{ISO:Extended:Z}") |> elem(1)
    }
  end

end

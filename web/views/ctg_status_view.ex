defmodule OpenTeleCtg.CtgStatusView do
  @moduledoc ~S"""
  View for rendering the CTG status response.
  """

  use OpenTeleCtg.Web, :view

  @doc ~S"""
  Renders the CTG status view.
  """
  @spec render(String.t, %{status: %{ctgs: list}}) :: map
  def render("status.json", %{ctgs: ctgs}) when length(ctgs) > 1 do

    stati = Enum.map(ctgs, &build_status/1)
    references = Enum.map(ctgs, &(URI.decode_www_form(&1.external_id)))

    %{statuses: stati,
      links: %{
        self: ctg_status_url(OpenTeleCtg.Endpoint, :status,
          reference: references)
      }
    }
  end

  def render("status.json", %{ctgs: ctgs}) do
    [ctg | _] = ctgs

    %{statuses: [build_status(ctg)],
      links: %{
        self: ctg_status_url(OpenTeleCtg.Endpoint, :status,
          reference: URI.decode_www_form(ctg.external_id))
      }
    }
  end

  defp build_status(ctg) do
    %{
      status: ctg.status,
      reference: ctg.external_id,
      links: %{
        status: ctg_status_url(OpenTeleCtg.Endpoint, :status,
          reference: URI.decode_www_form(ctg.external_id))
      }
    }
  end

end

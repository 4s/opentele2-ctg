defmodule OpenTeleCtg.CtgView do
  @moduledoc ~S"""
  View for rendering the created CTG response.
  """

  use OpenTeleCtg.Web, :view

  @doc ~S"""
  Render the CTG created view.
  """
  @spec render(String.t, %{ctg: list}) :: map
  def render("created.json", %{ctg: ctg}) do
    %{links: %{
        status: ctg_status_url(OpenTeleCtg.Endpoint, :status,
          reference: ctg.external_id)
      }
    }
  end

end

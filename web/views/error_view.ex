defmodule OpenTeleCtg.ErrorView do
  @moduledoc ~S"""
  Module for rendering error views.
  """

  require Logger
  use OpenTeleCtg.Web, :view
  alias OpenTeleCtg.{ErrorBundle, ApiError}

  @doc ~S"""
  Render a 401 error response view.
  """
  @spec render(String.t, map | ErroBundle.t) :: map
  def render("401.json", _) do
    %{message: "Unauthorized"}
  end

  def render("404.json", %{resource: resource_name}) do
    resource_name
    |> build_resource_not_found
    |> render_error
  end

  def render("404.json", _) do
    "unknown"
    |> build_resource_not_found
    |> render_error
  end

  def render("422.json", errors) do
    errors
    |> render_error
  end

  def render("500.json", errors) do
    if Map.has_key?(errors, :message) do
      Logger.debug "ERRORS: #{inspect errors}"
      errors
      |> render_error
    else
      %{message: "Internal server error"}
    end
  end

  @doc ~S"""
  Renders an error view.
  """
  @spec render_error(ErrorBundle.t) :: map
  def render_error(error_wrapper) do
    %{message: error_wrapper.message,
      errors: to_json(error_wrapper.errors)}
  end

  @spec to_json([%ApiError{}]) :: [map]
  defp to_json(errors) do
    errors
    |> Enum.map(fn error ->
      %{resource: error.resource, field: error.field, code: error.code}
    end)
  end

  @doc ~S"""
  Renders a 'template not found' error view.
  """
  @spec template_not_found(any, map) :: map
  def template_not_found(_template, %{}) do
    render("500.json", %{})
  end

end

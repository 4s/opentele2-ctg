defmodule OpenTeleCtg.JSONSchema.Cache do
  @moduledoc ~S"""
  Caches JSON schema files after first request.
  """

  require Logger
  @app_dir Application.app_dir(:open_tele_ctg)
  alias ExJsonSchema.Schema

  @doc ~S"""
  Initializes the JSON schema cache.
  """
  @spec init(any) :: {:ok, list}
  def init(_opts) do
    {:ok, []}
  end

  @doc ~S"""
  Starts the JSON schema cache process.
  """
  @spec start_link :: :on_start
  def start_link do
    Logger.debug("Starting Schema cache...")
    Agent.start_link(fn -> %{} end, name: __MODULE__)
  end

  @doc ~S"""
  Looks up the schema with the specified name.
  """
  @spec get_schema(String.t) :: Schema.Root.t | :ok
  def get_schema(schema_name) do
    schema = Agent.get(__MODULE__, fn map -> Map.get(map, schema_name) end)
    case schema do
      nil -> resolve_schema(schema_name)
      _ -> schema
    end
  end

  @spec resolve_schema(String.t) :: Schema.Root.t | :ok
  defp resolve_schema(schema_name) do
    Logger.debug("Schema '#{schema_name}' not found in cache, resolving by reading file")

    with {:ok, schema_file} <- (File.read "#{@app_dir}/priv/static/json_schemas/#{schema_name}.json"),
         {:ok, schema_json_content} <- (Poison.decode schema_file),
           resolved_schema <- (Schema.resolve schema_json_content) do

      Logger.debug "Successfully resolved schema: '#{schema_name}'"
      cache_schema(schema_name, resolved_schema)
      resolved_schema

    else
      error ->
        Logger.error("Error occurred: '#{inspect error}' trying to resolve " <>
          "schema: '#{schema_name}'")
    end
  end

  @doc ~S"""
  Caches the specified schema with the specified name.
  """
  @spec cache_schema(String.t, Schema.Root.t) :: :ok
  def cache_schema(schema_name, resolved_schema) do
    Agent.update(__MODULE__, fn map ->
      Map.put(map, schema_name, resolved_schema)
    end)
  end

end

defmodule OpenTeleCtg.JSONSchema.Validator do
  @moduledoc ~S"""
  Validates an incoming JSON body against its associated JSON schema.
  """

  require Logger
  use GenServer
  alias OpenTeleCtg.JSONSchema.Cache
  alias OpenTeleCtg.ApiError
  alias ExJsonSchema.{Schema, Validator}

  @doc ~S"""
  Initializes the JSON schema validator.
  """
  @spec init(any) :: {:ok, list}
  def init(_opts) do
    {:ok, []}
  end

  @doc ~S"""
  Starts the JSON schema validator.
  """
  @spec start_link :: :on_start
  def start_link do
    GenServer.start(__MODULE__, [], name: __MODULE__)
  end

  @doc ~S"""
  Validates a JSON schema.
  """
  @spec validate(map) :: term
  def validate(%{data: data, path: path}) do
    GenServer.call(__MODULE__, {:validate, data, path})
  end

  @doc ~S"""
  GenServer function for validating a JSON schema.
  """
  @spec handle_call({atom, map, String.t}, any, any)
  :: :ok | {:error, [ApiError.t]}
  def handle_call({:validate, ctg_data, "ctg"}, _from, _names) do
    validate_result("create-ctg", ctg_data, "ctg")
  end

  def handle_call({:validate, continuous_ctg_data, "continuous_ctg"}, _from, _names) do
    validate_result("create-continuous-ctg", continuous_ctg_data, "continuous_ctg")
  end

  @spec validate_result(String.t, map, String.t)
  :: :ok | {:error, [ApiError.t]}
  defp validate_result(schema_title, schema_data, resource) do
    Logger.debug "schema_title: #{schema_title}"
    Logger.debug "schema_data: #{inspect schema_data}"

    schema = Cache.get_schema(schema_title)

    if schema do
      validation_result = get_validation_result(schema, schema_data)
      Logger.debug "Validation result: #{inspect validation_result}"

      validation_result =
        validation_result
        |> build_validation_errors(resource)

      {:reply, validation_result, schema}

    else
      {:reply,
       {:error, %ApiError{resource: resource,
                          field: "",
                          code: "Could not find JSON schema for resource"}},
       schema}
    end
  end

  @spec get_validation_result(Schema.Root.t, map)
  :: :ok | {:error, (list | String.t)}
  defp get_validation_result(schema, data) do
    try do
      Validator.validate(schema, data)
    rescue
      error ->
        Logger.error "Error occurred while validating : #{inspect error}"
      {:error, error}
    end
  end

  @spec build_validation_errors(:ok, String.t) :: :ok
  defp build_validation_errors(:ok, _), do: :ok

  @spec build_validation_errors({:error, list}, String.t)
  :: {:error, [ApiError.t]}
  defp build_validation_errors({:error, errors}, resource) do
    Logger.debug "Errors: #{inspect errors}"

    validation_errors =
      errors
      |> Enum.map(fn {code, field} ->
        field = field |> String.replace("#", "") |> String.replace("/", "")
        ApiError.new(resource, field, code)
      end)

      {:error, validation_errors}
  end

end

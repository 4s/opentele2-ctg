defmodule OpenTeleCtg.ApiError do
  @moduledoc ~S"""
  Represents an API error.

  Consists of a `:resource` id, `:field` id, and error `code`.
  """

  @type t :: %__MODULE__{resource: String.t,
                         field: String.t,
                         code: String.t}

  defstruct [
    :resource,
    :field,
    :code
  ]

  @spec new(String.t, String.t, String.t) :: t
  def new(resource, field, code) do
    %__MODULE__{resource: resource,
                field: field,
                code: code}
  end

end

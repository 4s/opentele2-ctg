defmodule OpenTeleCtg.CtgSample do
  @moduledoc ~S"""
  Represents a CTG sample event.

  A `CtgSample` event consists of:

  - a `:count` property, that is the count number of the sample,
  - a `:read_time` property, the UTC timestamp of when the sample was recorded,
  - a `:fhr` array, the fetal heart rate, measured in BPM,
  - a `:qfhr` array, the quality of the fetal heart rate, which is
    either 0 (red), 1 (yellow), or 2 (green), corresponding to the signal
    lamps on the Monica device,
  - a `:mhr` array, the maternal heart rate, measured in BPM, and
  - a `:toco` array, tocometry values (force of uterine contractions),
    measured on a scale from 0.0-1.0.
  """

  @type t :: %__MODULE__{count: integer,
                         read_time: DateTime.t,
                         fhr: [number],
                         mhr: [number],
                         qfhr: [integer],
                         toco: [number]}

  defstruct [
    :count,
    :read_time,
    :fhr,
    :mhr,
    :qfhr,
    :toco
  ]

  @spec from_params(map) :: t
  def from_params(%{"sampleCount" => count,
                    "readTime" => read_time_raw,
                    "fhr" => fhr,
                    "mhr" => mhr,
                    "qfhr" => qfhr,
                    "toco" => toco}) do

    read_time = Timex.parse!(read_time_raw, "{ISO:Extended:Z}")
    %__MODULE__{count: count,
                read_time: read_time,
                fhr: fhr,
                mhr: mhr,
                qfhr: qfhr,
                toco: toco}
  end

  @spec new(integer, DateTime.t, [number], [number], [integer], [integer]) :: t
  def new(count, read_time, fhr, mhr, qfhr, toco) do
    %__MODULE__{count: count,
                read_time: read_time,
                fhr: fhr,
                mhr: mhr,
                qfhr: qfhr,
                toco: toco}
  end

end

defmodule OpenTeleCtg.CtgStop do
  @moduledoc ~S"""
  Represents a CTG stop event.

  A `CtgStop` event consists of:

  - a `:stop_time` property, that is the stop time of the continuous CTG
    measurement as a UTC date time timestamp.
  """

  @type t :: %__MODULE__{stop_time: DateTime.t}

  defstruct [
    :stop_time
  ]

  @spec from_params(map) :: t
  def from_params(%{"stopTime" => stop_time_raw}) do
    stop_time = Timex.parse!(stop_time_raw, "{ISO:Extended:Z}")
    %__MODULE__{stop_time: stop_time}
  end

  @spec new(DateTime.t) :: t
  def new(stop_time) do
    %__MODULE__{stop_time: stop_time}
  end

end

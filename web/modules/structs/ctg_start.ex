defmodule OpenTeleCtg.CtgStart do
  @moduledoc ~S"""
  Represents a CTG start event.

  A `CtgStart` event consists of:

  - a `:device_name` property, the name of the device,
  - a `:registration_id`, which is a UUID for identifying the continuous CTG
    measurement series.
  - a `ua_delay` (also know as toco shift) property, representing the delay of
    the values in the toco series compared to the other measurement series (fhr
    etc.) measured in seconds (this is usually either 4s or 25s), and
  - a `ua_sensitivity` property, which is either "high" or "low".
  """

  @type t :: %__MODULE__{device_name: String.t,
                         registration_id: String.t,
                         ua_delay: integer,
                         ua_sensitivity: String.t}

  defstruct [
    :device_name,
    :registration_id,
    :ua_delay,
    :ua_sensitivity
  ]

  @spec from_params(map) :: %OpenTeleCtg.CtgStart{}
  def from_params(%{"deviceName" => device_name,
                    "registrationIdentifier" => registration_id,
                    "uaDelay" => ua_delay,
                    "uaSensitivity" => ua_sensitivity
                   }) do
    %__MODULE__{device_name: device_name,
                registration_id: registration_id,
                ua_delay: ua_delay,
                ua_sensitivity: ua_sensitivity}
  end

  @spec new(String.t, String.t, integer, DateTime.t) :: t
  def new(device_name, registration_id, ua_delay, ua_sensitivity) do
    %__MODULE__{device_name: device_name,
                registration_id: registration_id,
                ua_delay: ua_delay,
                ua_sensitivity: ua_sensitivity}
  end

end

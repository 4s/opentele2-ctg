defmodule OpenTeleCtg.ErrorBundle do
  @moduledoc ~S"""
  Represents an error bundle.

  An `ErrorBundle` consists of a message and a series of errors.
  """

  @type t :: %__MODULE__{message: String.t,
                         errors: list}

  defstruct [
    :message,
    :errors
  ]

  @spec new(String.t, list) :: t
  def new(message, errors) do
    %__MODULE__{message: message,
                errors: errors}
  end

end

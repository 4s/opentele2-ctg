defmodule OpenTeleCtg.PatientReference do
  @moduledoc ~S"""
  Represents a patient reference to a patient saved in OpenTele.

  A `PatientReference` consists of:

  - a `:unique_id` property, e.g. the CPR number of the patient,
  - a `:first_name`, and
  - a `:last_name`.

  """

  @type t :: %__MODULE__{unique_id: String.t,
                         first_name: String.t,
                         last_name: String.t}

  defstruct [
    :unique_id,
    :first_name,
    :last_name
  ]

  @spec from_params(map) :: t
  def from_params(%{"uniqueId" => unique_id,
                    "firstName" => first_name,
                    "lastName" => last_name}) do
    %__MODULE__{unique_id: unique_id,
                first_name: first_name,
                last_name: last_name}
  end

  @spec new(String.t, String.t, String.t) :: t
  def new(unique_id, first_name, last_name) do
    %__MODULE__{unique_id: unique_id,
                first_name: first_name,
                last_name: last_name}
  end

end

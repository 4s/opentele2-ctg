defmodule OpenTeleCtg.MilouEndpoint do
  @moduledoc ~S"""
  Represents a Milou endpoint.

  A `MilouEndpoint` consists of:

  - a `:unique_id` property, the unique ID of a patient,
  - a `:queue` property, a queue of CTG events,
  - a `:meta` property, which contains meta information extracted from a
    `CtgStart` event, i.e. `:device_name`, `:registration_id`, and `:ua_delay`.
  - a `:stop_event` property, a `CtgStop` event, and
  - a `:exporter` property, which is the `pid` of the process that sends the
    events to Milou.
  """

  alias OpenTeleCtg.CtgStop

  @type t :: %__MODULE__{unique_id: String.t,
                         queue: list,
                         meta: map,
                         stop_event: CtgStop.t,
                         exporter: pid}

  defstruct [
    :unique_id,
    :queue,
    :meta,
    :stop_event,
    :exporter
  ]

  @spec new(String.t, list, map, CtgStop.t, pid) :: t
  def new(unique_id, queue, meta, stop_event, exporter) do
    %__MODULE__{unique_id: unique_id,
                queue: queue,
                meta: meta,
                stop_event: stop_event,
                exporter: exporter}
  end


end

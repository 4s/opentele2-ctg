defmodule OpenTeleCtg.CtgSignal do
  @moduledoc ~S"""
  Represents a CTG signal event.

  A `CtgSignal` event consists of:

  - a `:signal_time` property, that is a UTC timestamp corresponding to a point
    in time where the patient pushed the button on the Monica device.
  """

  @type t :: %__MODULE__{signal_time: DateTime.t}

  defstruct [
    :signal_time
  ]

  @spec from_params(map) :: t
  def from_params(%{"signalTime" => signal_time_raw}) do
    signal_time = Timex.parse!(signal_time_raw, "{ISO:Extended:Z}")
    %__MODULE__{signal_time: signal_time}
  end

  @spec new(DateTime.t) :: t
  def new(signal_time) do
    %__MODULE__{signal_time: signal_time}
  end

end

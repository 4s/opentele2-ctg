defmodule OpenTeleCtg.OpenTeleServer.RolePermission do
  @moduledoc ~S"""
  Represents a relation between a role and a permission
  in the opentele-server database.
  """

  use OpenTeleCtg.Web, :model

  @primary_key false
  schema "role_permission" do
    field :role_id, :integer
    field :permission_id, :integer
  end

end

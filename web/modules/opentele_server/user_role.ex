defmodule OpenTeleCtg.OpenTeleServer.UserRole do
  @moduledoc ~S"""
  Represents a user role in the opentele-server database.
  """

  use OpenTeleCtg.Web, :model

  @primary_key false
  schema "user_role" do
    field :user_id, :integer
    field :role_id, :integer
  end

end

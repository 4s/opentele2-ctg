defmodule OpenTeleCtg.OpenTeleServer.Permission do
  @moduledoc ~S"""
  Represents a permission in the opentele-server database.
  """

  use OpenTeleCtg.Web, :model

  schema "permission" do
    field :permission, :string
  end

end

defmodule OpenTeleCtg.OpenTeleServer.Repo do
  @moduledoc ~S"""
  The `Repo` corresponding to the opentele-server database.
  """
  use Ecto.Repo, otp_app: :open_tele_ctg
end

defmodule OpenTeleCtg.OpenTeleServer.User do
  @moduledoc ~S"""
  Represents a user in the opentele-server database.
  """

  use OpenTeleCtg.Web, :model

  schema "users" do
    field :username, :string
    field :password, :string
  end

end

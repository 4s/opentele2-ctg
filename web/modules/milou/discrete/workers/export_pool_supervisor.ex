defmodule OpenTeleCtg.Milou.Discrete.Workers.CtgExportPool.Supervisor do
  @moduledoc ~S"""
  Manages the pool of discrete CTG exporters.
  """

  use Supervisor

  @doc ~S"""
  Starts the CTG exporter `Supervisor`.
  """
  @spec start_link :: :on_start
  def start_link do
    Supervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  @doc ~S"""
  Initializes the CTG exporter `Supervisor`.
  """
  @spec init(list) :: {:ok, tuple}
  def init([]) do
    pool_options = [
      name: {:local, :export_ctg_pool},
      worker_module: OpenTeleCtg.Milou.Discrete.Workers.CtgExport,
      size: 10,
      max_overflow: 5,
    ]

    children = [
      :poolboy.child_spec(:export_ctg_pool, pool_options, [])
    ]

    supervise(children, strategy: :one_for_one)
  end
end

defmodule OpenTeleCtg.Milou.Discrete.Workers.CtgExport do
  @moduledoc ~S"""
  Worker responsible for taking a `Ctg`, turning it into SOAP XML and
  sending it to the Medexa Milou service.
  """

  use GenServer
  require Logger
  alias OpenTeleCtg.{Ctg, Soap}
  alias OpenTeleCtg.Milou.Discrete.Message.ImportCtg

  @doc ~S"""
  Given a `ctg` series, an `endpoint` description and a
  `millis_between_retries` option, turns the `CTG` series
  into SOAP XML and sends it to the Milou service.
  """
  @spec export([ctg: Ctg.t,
                endpoint: String.t,
                millis_between_retries: non_neg_integer()]) :: any
  def export(ctg: ctg, endpoint: endpoint,
    millis_between_retries: millis_between_retries) do

    Task.async(fn ->
      parameters = %{export: ctg,
                     to: endpoint,
                     millis_between_retries: millis_between_retries}

      :poolboy.transaction(:export_ctg_pool,
        &(GenServer.call(&1, parameters, :infinity)), :infinity)
    end)
  end

  @doc ~S"""
  Starts the underlying `GenServer` of the `CtgExport` process.
  """
  @spec start_link(list) :: :on_start
  def start_link([]) do
    GenServer.start_link(__MODULE__, [], [])
  end

  @doc ~S"""
  Initializes the `CtgExport` process.
  """
  @spec init(map) :: {:ok, map}
  def init(state) do
    {:ok, state}
  end

  @doc ~S"""
  `GenServer` callback for exporting CTG series.
  """
  @spec handle_call(map, any, any) :: {:reply, :ok, list}
  def handle_call(%{export: ctg,
                    to: endpoint,
                    millis_between_retries: millis_between_retries}, _, _) do

    export_ctg(ctg, endpoint, millis_between_retries)
    {:reply, :ok, []}
  end

  @spec export_ctg(%Ctg{}, String.t, integer) :: :ok | {:error, any}
  defp export_ctg(ctg, endpoint, millis_between_retries) do

    soap_message = ImportCtg.message(ctg)
    soap_action = "http://tempuri.org/ICtgImport/ImportCtg"
    endpoint = get_action_endpoint(endpoint, "ImportCtg")
    Logger.debug "SOAP message: #{inspect soap_message}"

    case Soap.do_export(endpoint, soap_message,
          soap_action, millis_between_retries) do
      :ok ->
        Ctg.update_to_exported(ctg)
        :ok

      {:error, reason} ->
        Logger.error "Failed to export ctg " <>
          "with external reference: '#{ctg.external_id}'"
        Ctg.update_to_failed(ctg)
        {:error, reason}
    end
  end

  @doc ~S"""
  Returns the endpoint for the given SOAP action.
  """
  @spec get_action_endpoint(String.t, String.t) :: String.t
  def get_action_endpoint(endpoint, action) do
    URI.to_string(URI.merge(endpoint, action))
  end

end

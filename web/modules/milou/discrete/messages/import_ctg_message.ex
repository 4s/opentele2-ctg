defmodule OpenTeleCtg.Milou.Discrete.Message.ImportCtg do
  @moduledoc ~S"""
  Plugs data into an `ImportCtg` SOAP message template.
  """

  @template_location Application.app_dir(:open_tele_ctg) <>
    "/priv/milou/xml_templates/milou_import_ctg.xml.eex"

  require EEx
  alias OpenTeleCtg.Ctg

  EEx.function_from_file(:defp, :template_for_import_ctg, @template_location,
    [:start_time, :gziped_ctgs, :signals, :device_name, :end_time,
     :patient_external_id, :patient_first_name, :patient_last_name, :ua_delay])

  @doc ~S"""
  Returns a string representation of the SOAP CTG measurement series message
  to be sent to Milou.
  """
  @spec message(Ctg.t) :: String.t
  def message(ctg) do

    ctg = Ctg.load_associations(ctg)

    sorted_samples = Enum.sort(ctg.samples,
      &(&1.sequence_number < &2.sequence_number))

    fhr = Enum.map(sorted_samples, fn (sample) -> sample.fhr end)
    mhr = Enum.map(sorted_samples, fn (sample) -> sample.mhr end)
    toco = Enum.map(sorted_samples, fn (sample) -> sample.toco end)
    qfhr = Enum.map(sorted_samples, fn (sample) -> sample.qfhr end)

    gziped_ctgs = create_gziped_content(fhr, mhr, toco, qfhr)

    start_time = ctg.start_time
    signals = Enum.map(ctg.signals, &(&1.when))
    device_name = ctg.device_name
    end_time = ctg.end_time
    patient_id = ctg.patient_external_id
    first_name = ctg.patient_first_name
    last_name = ctg.patient_last_name
    ua_delay = ctg.ua_delay

    template_for_import_ctg(start_time, gziped_ctgs, signals, device_name,
      end_time, patient_id, first_name, last_name, ua_delay)
  end

  @doc ~S"""
  Gzips and base64 encodes the CTG measurement series values.
  """
  @spec create_gziped_content(list, list, list, list) :: String.t
  defp create_gziped_content(fhr, mhr, toco, qfhr) do
     [fhr, mhr, toco, qfhr]
     |> List.zip() #Turns list into list of tuples: [{fhr1, mhr1, toco1, qfhr1}, {fhr2, mhr2...}, ...]
     |> Enum.map(&Tuple.to_list/1) #Turns list of tuples into list of lists: [[fhr1, mhr1, toco1, qfhr1], [fhr2, mhr2...], ...]
     |> Enum.map(&(Enum.join(&1, " ,"))) #Turns list of lists into list of Strings: ["fhr1, mhr1, toco1, qfhr1", "fhr2, mhr2...", ...]
     |> Enum.map(&("[#{&1}]")) #Wraps each string with begin and end square braces: [["fhr1, mhr1, toco1, qfhr1"], ["fhr2, mhr2..."], ...]
     |> Enum.join("\n") #Joins list of strings into single string "[fhr1, mhr1, toco1, qfhr1]\n [fhr2, mhr2...]\n ..."
     |> :zlib.gzip() #gzip compresses the string
     |> Base.encode64() #Base64 encodes the gziped string
  end
end

defmodule OpenTeleCtg.Milou.Discrete do
  @moduledoc ~S"""
  Module for exporting complete CTG series for a patient to a Milou backend.

  Does so by sending measurement series to a pool of exporters.
  """

  require Logger
  use GenServer
  alias OpenTeleCtg.Ctg
  alias OpenTeleCtg.Milou.Discrete.Workers.CtgExport

  @doc ~S"""
  Exports a complete `Ctg` measurement series to Milou.
  """
  @spec export([ctg: Ctg.t]) :: :ok
  def export(ctg: ctg) do
    GenServer.cast(__MODULE__, {:export, ctg})
  end

  @doc ~S"""
  Starts the `Discrete` CTG export process.
  """
  @spec start_link :: :on_start
  def start_link do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  @doc ~S"""
  Initializes the `Discrete` CTG export process.
  """
  @spec init(list) :: {:ok, list}
  def init([]) do
   {:ok, []}
  end

  @doc ~S"""
  `GenServer` callback for handling export requests.

  Forwards the specified `Ctg` measurement series to the `CtgExport` module.
  """
  @spec handle_cast({:export, Ctg.t}, list) :: {:noreply, list}
  def handle_cast({:export, ctg}, []) do

    milou_config = Application.get_env(:open_tele_ctg, OpenTeleCtg.Milou)
    endpoint = Keyword.fetch!(milou_config, :milou_discrete_endpoint)
    millis_between_retries = Keyword.fetch!(
      milou_config, :discrete_millis_between_retries)

    Logger.info "Starting Export of CTG with external reference " <>
      "'#{ctg.external_id}' to '#{endpoint}'"
    CtgExport.export(ctg: ctg, endpoint: endpoint,
      millis_between_retries: millis_between_retries)

    {:noreply, []}
  end

  @doc ~S"""
  Goes through all `Ctg` measurement series that haven't yet been exported and
  sends them to the `CtgExport`.
  """
  @spec pick_up_pending_exports :: :ok
  def pick_up_pending_exports do
    Enum.each(Ctg.pending_export(), &(export(ctg: &1)))
  end

end

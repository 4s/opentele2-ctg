defmodule OpenTeleCtg.Milou.Continuous.Exporter.Supervisor do
  @moduledoc ~S"""
  Supervisor for CTG exporters.
  """

  use Supervisor
  alias OpenTeleCtg.Milou.Continuous.Exporter

  # A simple module attribute that stores the supervisor name
  @name OpenTeleCtg.Milou.Continuous.Exporter.Supervisor

  @spec start_link :: :on_start
  def start_link do
    Supervisor.start_link(__MODULE__, :ok, name: @name)
  end

  @spec start_exporter :: :on_start_child
  def start_exporter do
    Supervisor.start_child(@name, [])
  end

  @spec init(:ok) :: {:ok, tuple}
  def init(:ok) do
    children = [
      worker(Exporter, [], restart: :temporary)
    ]

    supervise(children, strategy: :simple_one_for_one)
  end

end

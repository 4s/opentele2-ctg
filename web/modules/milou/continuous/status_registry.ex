defmodule OpenTeleCtg.Milou.Continuous.StatusRegistry do
  @moduledoc ~S"""
  Keeps a registry of currently running continuous CTGs
  and the selected ua sensitivity.
  """

  require Logger
  alias OpenTeleCtg.PatientReference

  @doc ~S"""
  Initializes the `StatusRegistry`.
  """
  @spec init(any) :: {:ok, list}
  def init(_opts) do
    {:ok, []}
  end

  @doc ~S"""
  Starts the `StatusRegistry` process.
  """
  @spec start_link :: :ok
  def start_link do
    Logger.debug("Starting #{__MODULE__}...")
    Agent.start_link(fn -> %{} end, name: __MODULE__)
  end

  @doc ~S"""
  Registers that the patient identified by the `patient_reference`
  has started a continuous CTG with give sensitivity.

  Always returns `:ok`
  """
  @spec register(PatientReference.t, atom) :: :ok
  def register(%PatientReference{unique_id: unique_id}, sensitivity) do

    Logger.info("Registering continuous CTG for #{unique_id} " <>
      "with sensitvity #{sensitivity}")

    Agent.update(__MODULE__, fn map ->
      Map.put(map, unique_id, {String.to_atom(sensitivity), Timex.now()})
    end)
  end

  @doc ~S"""
  Refreshes the registration of continuous CTG. Registrations that are
  not refreshed on a regular basis will be considered stopped by the
  `poll_status` function
  """
  @spec refresh(PatientReference.t) :: map
  def refresh(%PatientReference{unique_id: unique_id}) do

    Logger.info("Refreshing continuous CTG status for #{unique_id}")
    Agent.update(__MODULE__, fn map ->

      if Map.has_key?(map, unique_id) do
        sensitivity =
          map
          |> Map.get(unique_id)
          |> elem(0)

        tuple = {sensitivity, Timex.now()}
        Map.put(map, unique_id, tuple)
      else
        map
      end
    end)
  end

  @doc ~S"""
  Removes the registration of a running continuous CTG for the given patient.
  """
  @spec unregister(PatientReference.t) :: :ok
  def unregister(%PatientReference{unique_id: unique_id}) do
    Logger.info("Refreshing continuous CTG status for #{unique_id}")
    Agent.update(__MODULE__, fn map -> Map.drop(map, [unique_id]) end)
  end

  @doc ~S"""
  Polls to see if any continus CTGs are currently running for the given patient.
  Returns:
     {:ok, :none} if no status is found
     {:ok, selected_sensitvity, last_sample_time} when a patient
         has a continuous ctg running
     {:ok, :timeout, last_sample_time} when a timeout has been detected
         (there has not been any calls to `refresh` within the deadline)
  """
  @spec poll_status(PatientReference.t)
  :: {:ok, :none} | {:ok, String.t, String.t} | {:ok, :timeout, String.t}
  def poll_status(%PatientReference{ unique_id: unique_id }) do
    Logger.debug("Polling for continuous CTG status for #{ unique_id }")
    Agent.get(__MODULE__,
      fn map ->
        Map.get(map, unique_id) |> handle_status()
      end)
  end

  @spec handle_status(nil | {String.t, String.t})
  :: {:ok, String.t, String.t} | {:ok, :timeout, String.t}
  defp handle_status(nil), do: { :ok, :none }
  defp handle_status({sensitvity, timestamp}) do

    millis_before_timeout =
      Application.get_env(:open_tele_ctg,
        OpenTeleCtg.Milou)[:continuous_ctg_millis_before_reporting_dead]

    considered_stopped_deadline =
      Timex.shift(timestamp, milliseconds: millis_before_timeout)

    unless Timex.after?(Timex.now(), considered_stopped_deadline) do
      {:ok, sensitvity, timestamp}
    else
      {:ok, :timeout, timestamp}
    end
  end

end

defmodule OpenTeleCtg.Milou.Continuous.Registry do
  @moduledoc ~S"""
  Maps patient CTG sessions to CTG exporters (one-to-one).
  """

  require Logger
  use GenServer
  alias OpenTeleCtg.PatientReference
  alias OpenTeleCtg.Milou.Continuous.Exporter

  ## Types

  @type state :: {map, map}

  ## Client API

  @doc ~S"""
  Starts the registry with the given `name`.
  """
  @spec start_link(String.t) :: term
  def start_link(name) do
    GenServer.start_link(__MODULE__, name, name: name)
  end

  @doc """
  Stops the registry.
  """
  @spec stop(pid) :: :ok
  def stop(server) do
    GenServer.stop(server)
  end

  @doc """
  Creates a CTG exporter for the given `patient_reference`.

  Returns `{:ok, pid}` if the exporter is created, `:error` otherwise.
  """
  @spec create(atom, %PatientReference{}) :: {:ok, pid} | :error
  def create(server, patient_reference) do
    GenServer.call(server, {:create, patient_reference})
  end

  @doc """
  Looks up the exporter pid for `patient_reference` stored in `server`.

  Returns `{:ok, pid}` if the exporter exists, `:error` otherwise.
  """
  @spec lookup(atom, %PatientReference{}) :: {:ok, pid} | :error
  def lookup(server, patient_reference) when is_atom(server) do
    GenServer.call(server, {:lookup, patient_reference})
  end

  ## Server Callbacks

  @doc ~S"""
  Initializes the underlying `GenServer`.
  """
  @spec init(atom) :: {:ok, map}
  def init(_name) do

    exporters = %{}
    refs = %{}
    {:ok, {exporters, refs}}
  end

  @doc ~S"""
  Generic callback for handling the various messages sent to the `GenServer`.
  """
  @spec handle_call({:lookup, PatientReference.t}, any, state)
  :: {:reply, {:ok, pid} | :error, state}
  def handle_call({:lookup, patient_reference}, _from,
    {exporters, _refs} = state) do

    unique_id = patient_reference.unique_id
    {:reply, Map.fetch(exporters, unique_id), state}
  end

  @spec handle_call({:create, PatientReference.t}, any, state)
  :: {:reply, pid, state}
  def handle_call({:create, patient_reference}, _from, state) do
    find_or_create(patient_reference, state)
  end

  @spec find_or_create(PatientReference.t, state)
  :: {:reply, {:ok, pid}, state}
  defp find_or_create(patient_reference, {exporters, refs}) do

    unique_id = patient_reference.unique_id
    case Map.fetch(exporters, unique_id) do
      {:ok, exporter_pid} ->
        {:reply, {:ok, exporter_pid}, {exporters, refs}}

      :error ->
        {:ok, exporter_pid} = Exporter.Supervisor.start_exporter
        ref = Process.monitor(exporter_pid)
        refs =
          refs
          |> Map.put(ref, unique_id)

        exporters = Map.put(exporters, unique_id, exporter_pid)
        {:reply, {:ok, exporter_pid}, {exporters, refs}}
    end
  end

  @doc ~S"""
  Callback for handling info messages.
  """
  @spec handle_info({:DOWN, reference, :process, pid, any}, state)
  :: {:noreply, state}
  def handle_info({:DOWN, ref, :process, pid, reason}, {exporters, refs}) do

    Logger.info "Process down: ref: #{inspect ref}, " <>
      "pid: #{inspect pid}, " <>
      "reason: #{inspect reason}"

    {unique_id, refs} = Map.pop(refs, ref)
    exporters = Map.delete(exporters, unique_id)
    {:noreply, {exporters, refs}}
  end

  @spec handle_info(any, any) :: {:noreply, state}
  def handle_info(_msg, state) do
    {:noreply, state}
  end

end

defmodule OpenTeleCtg.Milou.Continuous.Exporter do
  @moduledoc ~S"""
  Module for exporting CTG events for a specific patient measurement session.
  """

  require Logger
  alias OpenTeleCtg.{CtgStart, CtgStop, CtgSample, CtgSignal,
                     PatientReference, MilouEndpoint, Soap}
  alias OpenTeleCtg.Milou.Continuous.{NewMessage, StopRegistration}

  @type ctg_event :: (CtgSample.t | CtgSignal.t)

  @doc ~S"""
  Starts a new exporter and creates an event queue.
  """
  @spec start_link :: :on_start
  def start_link do
    Agent.start_link(fn ->
      %MilouEndpoint{unique_id: nil,
                     queue: [],
                     meta: nil,
                     stop_event: nil,
                     exporter: nil}
    end)
  end

  @doc ~S"""
  Initializes the exporter's Milou endpoint and starts exporting events.
  """
  @spec start(pid, PatientReference.t, CtgStart.t) :: :ok
  def start(exporter, patient_reference, ctg_start) do
    Agent.update(exporter, fn milou_endpoint ->

      unique_id = patient_reference.unique_id
      meta = %{device_name: ctg_start.device_name,
               registration_id: ctg_start.registration_id,
               ua_delay: ctg_start.ua_delay}

    {:ok, pid} = Task.start_link(fn ->
        milou_config = Application.get_env(:open_tele_ctg,
        OpenTeleCtg.Milou, nil)

        endpoint = Keyword.fetch!(milou_config, :milou_continuous_endpoint)
        millis_between_retries = Keyword.fetch!(milou_config,
          :continuous_millis_between_retries)

        last_export_time = Timex.now()
        export_config = %{endpoint: endpoint,
                          millis_between_retries: millis_between_retries}

        send_pending_events(exporter, patient_reference, meta,
          export_config, last_export_time)
      end)

    %{milou_endpoint |
      unique_id: unique_id,
      meta: meta,
      exporter: pid}
    end)
  end

  @doc ~S"""
  Stops the exporter process.
  """
  @spec stop(pid, CtgStop.t) :: :ok
  def stop(exporter, ctg_stop) do
    Agent.update(exporter, fn milou_endpoint ->
      Logger.debug "Stop event received: #{inspect ctg_stop}"
      %{milou_endpoint | stop_event: ctg_stop}
    end)
  end

  @doc ~S"""
  Adds a list of CTG events to the exporter's queue.
  """
  @spec enqueue(pid, [ctg_event]) :: MilouEndpoint.t
  def enqueue(exporter, events) do
    Agent.update(exporter, fn milou_endpoint ->
      old_queue = milou_endpoint.queue
      new_queue = old_queue ++ events
      %{milou_endpoint | queue: new_queue}
    end)
  end

  @doc ~S"""
  Removes all events from the queue.
  """
  @spec dequeue(pid) :: {[ctg_event], MilouEndpoint.t}
  def dequeue(exporter) do
    Agent.get_and_update(exporter, fn milou_endpoint ->
      queue = milou_endpoint.queue
      milou_endpoint = %{milou_endpoint | queue: []}
      {queue, milou_endpoint}
    end)
  end

  @doc ~S"""
  Fetches the `CtgStop` event of the specified exporter.
  """
  @spec get_stop_event(pid) :: CtgStop.t
  def get_stop_event(exporter) do
    Agent.get(exporter, fn milou_endpoint ->
      milou_endpoint.stop_event
    end)
  end

  @doc ~S"""
  Sends any pending events on the queue to the Milou backend.
  """
  @spec send_pending_events(pid, PatientReference.t, map, map, DateTime.t)
  :: none
  def send_pending_events(exporter, patient_reference, meta,
    export_config, last_export_time) do

    sleep_time = 2000
    minutes_before_being_killed = 10

    ctg_events = dequeue(exporter)
    # Logger.debug("CTG events: #{inspect ctg_events}")

    if Enum.empty? ctg_events do
      ctg_stop = get_stop_event(exporter)
      # Logger.debug("CTG stop: #{inspect ctg_stop}")
      killing_time =
        Timex.now
        |> Timex.shift(minutes: minutes_before_being_killed)

      cond do
        ctg_stop ->
          stop_export(ctg_stop, meta, export_config)
          Agent.stop(exporter)

        Timex.after?(last_export_time, killing_time) ->
          Agent.stop(exporter)

        true ->
          :timer.sleep(sleep_time)
          send_pending_events(exporter, patient_reference, meta,
            export_config, last_export_time)
      end

    else
      ctg_events
      |> export_ctg(patient_reference, meta, export_config)

      updated_last_export_time = Timex.now()
      send_pending_events(exporter, patient_reference, meta,
        export_config, updated_last_export_time)
    end
  end

  @spec export_ctg([ctg_event], PatientReference.t, map, map) :: any
  defp export_ctg(ctg_events, patient_reference, meta, export_config) do

    soap_message = NewMessage.message(ctg_events, patient_reference, meta)
    soap_action = "http://tempuri.org/IOpenTeleRT/NewMessage"
    endpoint = get_action_endpoint(export_config.endpoint, "OpenTeleRT/")

    Logger.debug "Soap message: #{soap_message}"
    Soap.do_export(endpoint, soap_message, soap_action,
      export_config.millis_between_retries)
  end

  @spec stop_export(CtgStop.t, map, map) :: any
  defp stop_export(ctg_stop, meta, export_config) do

    soap_message = StopRegistration.message(ctg_stop, meta)
    soap_action = "http://tempuri.org/IOpenTeleRT/StopRegistration"
    endpoint = get_action_endpoint(export_config.endpoint, "OpenTeleRT/")

    Logger.debug "SOAP message: #{soap_message}"
    Soap.do_export(endpoint, soap_message, soap_action,
      export_config.millis_between_retries)
  end

  @doc ~S"""
  Returns the endpoint for the given SOAP action.
  """
  @spec get_action_endpoint(String.t, String.t) :: String.t
  def get_action_endpoint(endpoint, action) do
    URI.to_string(URI.merge(endpoint, action))
  end

end

defmodule OpenTeleCtg.Milou.Continuous do
  @moduledoc ~S"""
  Module for exporting CTG events for a specific patient.

  Does so by starting and stopping exporters and forwarding CTG events
  to exporters.
  """

  require Logger
  alias OpenTeleCtg.{PatientReference, CtgStart, CtgStop, CtgSample, CtgSignal}
  alias OpenTeleCtg.Milou.Continuous.{Registry, Exporter, StatusRegistry}

  @doc ~S"""
  Given a `PatientReference` and a `CtgStart` event, starts a continuous CTG
  exporting session by firing up an `Exporter` process and registering
  the ongoing session in a `Registry`.
  """
  @spec start(PatientReference.t, CtgStart.t)
  :: :ok | {:error, :could_not_create}
  def start(patient_reference, ctg_start) do
    Logger.error(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

    case Registry.create(Registry, patient_reference) do
      {:ok, exporter_pid} ->
        Exporter.start(exporter_pid, patient_reference, ctg_start)
        StatusRegistry.register(patient_reference, ctg_start.ua_sensitivity)

      :error ->
        {:error, :could_not_create}
    end
  end

  @doc ~S"""
  Given a `PatientReference` and a list of `CtgSample` and `CtgSignal` events,
  looks up the exporter process associated with the `PatientReference` and
  puts the events on the exporter's queue.
  """
  @spec export(PatientReference.t, [(CtgSample.t | CtgSignal.t)])
  :: :ok | {:error, :not_found}
  def export(patient_reference, ctg_samples_and_signals) do

    case lookup(patient_reference) do
      {:ok, exporter_pid} ->
        Exporter.enqueue(exporter_pid, ctg_samples_and_signals)

      {:error, :not_found} ->
        {:error, :not_found}
    end
  end

  @doc ~S"""
  Given a `PatientReference` and a `CtgStop` event, stops the exporter process
  associated with the `PatientReference`.
  """
  @spec stop(PatientReference.t, CtgStop.t) :: :ok | {:error, :not_found}
  def stop(patient_reference, ctg_stop) do

    case lookup(patient_reference) do
      {:ok, exporter_pid} ->
        Exporter.stop(exporter_pid, ctg_stop)

      {:error, :not_found} ->
        {:error, :not_found}
    end
  end

  @spec lookup(PatientReference.t) :: {:ok, pid} | {:error, :not_found}
  defp lookup(patient_reference) do

    case Registry.lookup(Registry, patient_reference) do
      {:ok, exporter_pid} ->
        {:ok, exporter_pid}

      :error ->
        {:error, :not_found}
    end
  end

end

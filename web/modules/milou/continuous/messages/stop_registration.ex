defmodule OpenTeleCtg.Milou.Continuous.StopRegistration do
  @moduledoc ~S"""
  Plugs data into a `StopRegistration` SOAP message template.
  """

  @template_location Application.app_dir(:open_tele_ctg) <>
    "/priv/milou/xml_templates/milou_stop_registration.xml.eex"

  require EEx
  alias OpenTeleCtg.CtgStop

  EEx.function_from_file(:defp, :template_for_stop_registration, @template_location,
    [:stop, :meta])


  @doc ~S"""
  Returns a string representation of the SOAP CTG measurement stop message
  to be sent to Milou based on the given `ctg_stop` event.
  """
  @spec message(CtgStop.t, map) :: String.t
  def message(ctg_stop, meta) do
    ctg_stop = %{ctg_stop |
                  stop_time: to_datetime_string(ctg_stop.stop_time)}
    template_for_stop_registration(ctg_stop, meta)
  end

  @spec to_datetime_string(DateTime.t) :: String.t
  defp to_datetime_string(datetime) do
    datetime |> Timex.format("{ISO:Extended:Z}") |> elem(1)
  end

end

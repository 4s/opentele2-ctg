defmodule OpenTeleCtg.Milou.Continuous.NewMessage do
  @moduledoc ~S"""
  Plugs data into a `NewMessage` SOAP message template.
  """

  @template_location Application.app_dir(:open_tele_ctg) <>
    "/priv/milou/xml_templates/milou_new_message.xml.eex"

  require EEx
  require Logger
  alias OpenTeleCtg.{CtgSample, CtgSignal, PatientReference}

  @type ctg_event :: (CtgSample.t | CtgSignal.t)

  EEx.function_from_file(:defp, :template_for_new_message, @template_location,
    [:patient_reference, :meta, :samples, :signals])

  @doc ~S"""
  Returns a string representation of the SOAP CTG measurement events message
  to be sent to Milou based on the given list of `ctg_events`.
  """
  @spec message([ctg_event], PatientReference.t, map) :: String.t
  def message(ctg_events, patient_reference, meta) do

    {samples, signals} =
      ctg_events
      |> Enum.partition(fn ctg_event -> ctg_event.__struct__ == CtgSample end)

    samples = samples |> Enum.sort(fn (s1, s2) -> s1.count < s2.count end)

    samples = samples |> Enum.map(fn sample ->
      %{sample |
         fhr: to_float_strings(sample.fhr),
         mhr: to_float_strings(sample.mhr),
         toco: to_float_strings(sample.toco),
         qfhr: to_integer_strings(sample.qfhr),
         read_time: to_datetime_string(sample.read_time)}
    end)

    signals = signals |> Enum.sort(fn (signal1, signal2) ->
      Timex.before?(signal1.signal_time, signal2.signal_time)
    end)

    signals = signals |> Enum.map(fn signal ->
      %{signal |
        signal_time: to_datetime_string(signal.signal_time)}
    end)

    template_for_new_message(patient_reference, meta, samples, signals)
  end

  @spec to_datetime_string(DateTime.t) :: String.t
  defp to_datetime_string(datetime) do
    datetime |> Timex.format("{ISO:Extended:Z}") |> elem(1)
  end

  @spec to_float_strings([Float.t]) :: [String.t]
  defp to_float_strings(floats) do
    "#{inspect floats, char_lists: false}"
  end

  @spec to_integer_strings([Integer.t]) :: [String.t]
  defp to_integer_strings(integers) do
    "#{inspect integers, char_lists: false}"
  end

end

defmodule OpenTeleCtg.Soap do
  @moduledoc ~S"""
  Module for wrapping SOAP HTTP requests to Milou for
  exporting CTG measurements.
  """

  require Logger

  @doc ~S"""
  Exports the given `soap_message` to the `endpoint`.
  """
  @spec do_export(String.t, String.t, String.t, pos_integer, integer)
  :: :ok | {:error, any}
  def do_export(endpoint, soap_message, soap_action,
    millis_between_retries, retries_left \\ 3)

  def do_export(endpoint, _soap_message, _soap_action,
    _millis_between_retries, 0) do
    # Logger.error "Failed to send soap message: #{soap_message} " <>
    Logger.error "Failed to send soap message to endpoint: #{endpoint}."
    {:error, :could_not_create}
  end

  def do_export(endpoint, soap_message, soap_action,
    millis_between_retries, retries_left) do
    # Logger.debug "Sending SOAP message: #{soap_message} " <> "to endpoint: #{endpoint}"

    headers = [
      {"SOAPAction", soap_action},
      {"Content-Type", "text/xml"}
    ]

    case HTTPoison.post(endpoint, soap_message, headers) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        Logger.info "Response: #{inspect body}"
        :ok

      {:ok, %HTTPoison.Response{status_code: status_code, body: body}} ->
        Logger.warn "Failed to send message, to #{endpoint}, " <>
          "received status code: #{status_code} " <>
          "and response: #{inspect body}"
        :timer.sleep(millis_between_retries)
        do_export(endpoint, soap_message, soap_action, millis_between_retries, retries_left - 1)

      {:error, %HTTPoison.Error{reason: reason}} ->
        Logger.warn "Failed to send message to #{endpoint}, " <>
          "received error: #{inspect reason}"
        :timer.sleep(millis_between_retries)
        do_export(endpoint, soap_message, soap_action, millis_between_retries, retries_left - 1)
    end
  end

end

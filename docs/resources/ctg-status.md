# Group CTG Status

## CTG [/ctg/status]

Provides a way to check the transfer status of one or more CTG measurement
series. Specifically, it states whether the CTG measurement series has been
successfully sent to the - and received by - the Milou backend.

The `status` of a CTG measurement series can either be `initial` if it hasn't
been sent to Milou yet and `exported` if it has been successfully received by
Milou.

### Get the status of a CTG export [GET /ctg/status?reference[]={reference_id}]

+ Parameters

    + reference_id (string) -
    An external reference to a CTG measurement whose export status should be
    returned. The parameter can be repeated when requesting the status of more
    than one CTG measurement series. Likewise, only requesting the status of one
    CTG measurement series, the `[]` can be omitted from the parameter name, as
    shown in the example response body of the create [CTG](#ctg) resource.

+ Request

  + Headers

            Authorization: API key <api-key>
            Content-type: application/json

+ Response 200 (application/json)

    + Body

            {
                "statuses": [
                    {
                        "status": "exported",
                        "links": {
                            "status": "https://<host>/ctg/status/1"
                        }
                    },
                    {
                        "status": "initial",
                        "links": {
                            "status": "https://<host>/ctg/status/2"
                        }
                    }
                ],
                "links": {
                    "self": "https://<host>/ctg/status/reference[]=reference1&reference[]=reference2"
                }
            }

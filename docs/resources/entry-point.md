# Group Entry Point

## CTG API Root [/.]

Entry point for consuming the API.

This resource offers basic information about the API version, server environment
and links to CTG related resources. In addition a link to this documentation is
also included in the representation.

### Retrieve the Entry Point [GET]

+ Response 200 (application/json)
    + Body

            {
              "serverEnvironment": "dev",
              "apiVersion": "1.0.0",
              "links": {
                "self": "http://<host>/",
                "schemas": "http://<host>/schemas",
                "isAlive": "http://<host>/isAlive",
                "ctgStatus": "http://<host>/ctg/status",
                "ctg": "http://<host>/ctg",
                "continuousCtg": "http://<host>/continuous_ctg",
                "apiDoc": "http://<host>/docs/ctg-api.html"
              }
            }

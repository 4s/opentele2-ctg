# Group Continuous CTG Status

## CTG [/continuous_ctg/status]

Provides a way to check the transfer status of a patient's continuous CTG
measurement series. Specifically, it states whether the continuous CTG
measurement series is ongoing or not.

The `status` of a continuous CTG measurement series can either be `none` if
it hasn't started. It can be `active` if it is ongoing and sending to Milou,
or it can be `timeout` if it has timed out.

When the `status` is either `active` or `timeout`, the response also includes a
`last_event` property which is a ISO-8601 date time timestamp for when the last
CTG measurement was received. If the status is `active` it also includes a
`sensitivity` property which is a string value of either `"high"` or `"low"`.

### Get the status of a continuous CTG export [GET /continuous_ctg/status/{patient_unique_id}]

+ Parameters

    + patient_unique_id (string) -
    The unique ID of the patient whose continuous CTG measurement status should
    be received.

+ Request

  + Headers

            Authorization: API key <api-key>
            Content-type: application/json

+ Response 200 (application/json)

    + Body

            {
              "status": "active",
              "sensitivity": "high",
              "last_event": "2016-10-10T19:40:18Z"
            }

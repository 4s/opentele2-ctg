# Group JSON Schemas

## Schemas [/schemas]

Resource providing access to all JSON schemas used by the API to describe the
available resources.

### Retrieve List of Schemas [GET]

+ Response 200 (application/json)
    + Body

            {
              "self": "http://<host>/schemas",
              "patient-definitions": "http://<host>/json_schemas/patient-definitions.json",
              "create-ctg": "http://<host>/json_schemas/create-ctg.json",
              "create-continuous-ctg": "http://<host>/json_schemas/create-continuous-ctg.json"
            }

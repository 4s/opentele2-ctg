# Group CTG

## CTG [/ctg]

This resource allows posting of a complete CTG measurement series to be
forwarded to an external system capable of visualizing the CTG data. The
resource returns a pointer to check the transfer status of the submitted CTG
measurement series, as these are batch processed.

All string inputs should be UTF-8 strings, and will be converted to UTF-8
otherwise.

All timestamps are parsed as ISO-8601 UTC datetime timestamps, and will be
rejected if not possible.

The request body consists of a patient reference, `patientRef`, and the CTG data
payload, `data`.

The `patientRef` object consists of:

- a `uniqueId` property, corresponding to the patient's CPR number in
  OpenTele-server,
- `firstName` and `lastName` properties, and finally,
- a `links` object that contains a pointer for fetching the actual patient's
  data in OpenTele-server via its API.

The `data` object consists of:

- a `reference` property, which is a unique ID chosen and supplied by the
  requester, used when retrieving the status of a CTG measurement series,
- a `startTime` property, representing the starting time of the measurement
  series,
- a `endTime` property, representing the stop time of the measurement series,
- a `deviceName` property, the name of the device,
- a `uaDelay` (also know as toco shift) property, representing the delay of the
  values in the `toco` series compared to the other measurement series, (fhr`
  etc.) measured in seconds (this is usually either 4s or 25s),
- a `signals` array, where each entry is a UTC timestamp corresponding to a
  point in time where the patient pushed the button on the Monica device,
- a `fhr` array, the fetal heart rate, measured in BPM,
- a `qfhr` array, the quality of the fetal heart rate, which is either 0 (red),
  1 (yellow), or 2 (green), corresponding to the signal lamps on the Monica
  device,
- a `mhr` array, the maternal heart rate, measured in BPM, and
- a `toco` array, tocometry values (force of uterine contractions), measured on
  a scale from 0.0-1.0.

The API expects `fhr`, `qfhr`, `mhr` and `toco` to be of equal length.

No input validation is done of the actual measurement values except check that
they are indeed numbers.

### Create a new CTG [POST]

+ Request

  + Headers

            Authorization: API key <api-key>
            Content-Type: application/json
  + Body

            {
              "ctg": {
                "patientRef": {
                   "uniqueId":"2512484916",
                   "links": {
                      "patient":"https://<host>/oth/api/patients/17"
                   },
                   "lastName":"Doe",
                   "firstName":"Nany Ann"
                },
                "data": {
                  "reference": "my_reference",
                  "startTime": "2016-06-08T13:28:56.077Z",
                  "endTime": "2016-06-08T13:59:56.097Z",
                  "deviceName": "Monica AN240213",
                  "uaDelay": 5,
                  "signals": [
                    "2016-06-08T13:28:59.098Z",
                    "2016-06-08T13:29:06.098Z",
                    "2016-06-08T13:29:17.098Z",
                    "2016-06-08T13:29:30.098Z"
                  ],
                  "fhr": [
                    146.0, 146.0, 147.0, 147.0, 147.0, 147.0, 147.0, 147.0,
                    147.0, 147.0, 147.25, 147.25, 147.25, 147.25, 147.25,
                    147.25, 147.25, 147.25, 147.0, 147.0, 147.0, 147.0,
                    147.0, 147.0, 147.0, 147.0, 148.5, 148.5, 148.5, 148.5,
                    148.5, 148.5, 148.5, 148.5, 151.5, 151.5, 151.5, 151.5,
                    151.5, 151.5, 151.5, 151.5, 152.5, 152.5, 152.5, 152.5,
                    152.5, 152.5, 152.5, 152.5, 151.75, 151.75, 151.75,
                    ...
                  ],
                  "qfhr": [
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    ...
                  ],
                  "mhr": [
                    74.0, 74.0, 74.0, 74.0, 74.0, 74.0, 74.0, 74.0, 74.0,
                    74.0, 74.0, 74.0, 74.0, 74.0, 74.0, 74.0, 74.75, 74.75,
                    74.75, 74.75, 74.75, 74.75, 74.75, 74.75, 77.0, 77.0,
                    77.0, 77.0, 77.0, 77.0, 77.0, 77.0, 74.25, 74.25, 74.25,
                    74.25, 74.25, 74.25, 74.25, 74.25, 73.75, 73.75, 73.75,
                    73.75, 73.75, 73.75, 73.75, 73.75, 71.75, 71.75, 71.75,
                    71.75, 71.75, 71.75, 71.75, 71.75, 72.5, 72.5, 72.5,
                    ...
                  ],
                  "toco": [
                    27.0, 27.0, 27.0, 27.0, 27.0, 27.0, 27.0, 27.0, 26.0,
                    26.0, 26.0, 26.0, 26.0, 26.0, 26.0, 26.0, 24.5, 24.5,
                    24.5, 24.5, 24.5, 24.5, 24.5, 24.5, 24.0, 24.0, 24.0,
                    24.0, 24.0, 24.0, 24.0, 24.0, 22.5, 22.5, 22.5, 22.5,
                    22.5, 22.5, 22.5, 22.5, 21.5, 21.5, 21.5, 21.5, 21.5,
                    21.5, 21.5, 21.5, 20.0, 20.0, 20.0, 20.0, 20.0, 20.0,
                    20.0, 20.0, 18.0, 18.0, 18.0, 18.0, 18.0, 18.0, 18.0,
                  ]
                }
              }
            }

  + Schema

            <!-- include(../../priv/static/json_schemas/create-ctg.json) -->

+ Response 201 (application/json)

  + Body

            {
              "links": {
                "status": "http://<host>/ctg/status?reference=my_reference"
              }
            }


+ Response 422 (application/json)

  + Body

            {
              "message": "Schema validation failed",
              "errors": [
                {
                  "resource": "ctg",
                  "field": "patientRef",
                  "code": "Required property was not present."
                },
                {
                  "resource": "ctg",
                  "field": "data",
                  "cause": "Required property data was not present."
                }
              ]
            }

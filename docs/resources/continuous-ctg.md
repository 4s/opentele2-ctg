# Group Continuous CTG

## Continuous CTG [/continuous_ctg]

This resource allows posting of continuous CTG events to be forwarded to an
external system capable of visualizing the CTG data as it is received.

A continuous CTG measurement series consists of an initial CTG start event,
followed by a variable number of CTG sample events and CTG signal events, and
finally terminated with a CTG stop event.

The CTG start event is mandatory in order to register the device, UA delay and
registration ID for the measurement series, but the CTG stop event is optional
as the service will just stop the exporter process if it hasn't received
a CTG event in the last 10 minutes.

While the request body in the example below includes both a start and stop
event, these would in normal circumstances be sent in two different requests
separated by a series of requests containing just samples and signals.

All string inputs should be UTF-8 strings, and will be converted to UTF-8
otherwise.

All timestamps are parsed as ISO-8601 UTC datetime timestamps, and will be
rejected if not possible.

The request body consists of a patient reference, `patientRef`, and the CTG data
payload, `data`.

The `patientRef` object consists of:

- a `uniqueId` property, corresponding to the patient's CPR number in
  OpenTele-server, along with
- a `firstName` and a `lastName` property.

The `data` object consists of a list of events, where an event can be one of the
following types:

- `ctgStart`, which consists of:
  - a `deviceName` property, the name of the device,
  - a `uaDelay` (also know as toco shift) property, representing the delay of
    the values in the `toco` series compared to the other measurement series
    (`fhr` etc.) measured in seconds (this is usually either 4s or 25s),
  - a `uaSensitivity` property, which is either `"high"` or `"low"`, and
  - a `registrationIdentifier`, which is a UUID for identifying the continuous
    CTG measurement series.

- `ctgSample`, which consists of:
  - a `sampleCount` property, that is the count number of the sample,
    i.e. `1, 2, 3, 4, ...`,
  - a `readTime` property, the UTC timestamp of when the sample was recorded,
  - a `fhr` array, the fetal heart rate, measured in BPM,
  - a `qfhr` array, the quality of the fetal heart rate, which is either 0
    (red), 1 (yellow), or 2 (green), corresponding to the signal lamps on the
    Monica device,
  - a `mhr` array, the maternal heart rate, measured in BPM, and
  - a `toco` array, tocometry values (force of uterine contractions), measured
    on a scale from 0.0-1.0.

- `ctgSignal`, which consists of:
  - a `signalTime` property, that is a UTC timestamp corresponding to a point in
    time where the patient pushed the button on the Monica device.

- `ctgStop`, which consists of:
  - a `stopTime` property, that is the stop time of the continuous CTG
    measurement as a UTC date time timestamp.

The API expects `fhr`, `qfhr`, `mhr` and `toco` to be of equal length, which
should be 4 for a each sample.

No input validation is done of the actual measurement values except check that
they are indeed numbers.

### Add a Continuous CTG event [POST]

+ Request

  + Headers

            Authorization: Basic <username:password>
            Content-Type: application/json
  + Body

            {
                "continuousCtg": {
                    "patientRef": {
                        "uniqueId": "1234567890",
                        "firstName": "Nancy Ann",
                        "lastName": "Doe"
                    },
                    "data": [
                        {
                            "ctgStart": {
                                "deviceName": "AN24",
                                "uaDelay": 4,
                                "uaSensitivity": "high",
                                "registrationIdentifier": "1b62c080-b12e-4654-98f7-0f31d9c62e77"
                            }
                        },
                        {
                            "ctgSample": {
                                "sampleCount": 1,
                                "readTime": "2016-10-10T19:40:18Z",
                                "fhr": [ 146.0, 146.0, 147.0, 147.0 ],
                                "qfhr": [ 2, 2, 2, 2 ],
                                "mhr": [ 74.0, 74.0, 74.0, 74.0 ],
                                "toco": [ 27.0, 27.0, 27.0, 27.0 ]
                            }
                        },
                        {
                            "ctgSignal": {
                                "signalTime": "2016-10-10T19:41:18Z"
                            }
                        },
                        {
                            "ctgSample": {
                                "sampleCount": 2,
                                "readTime": "2016-10-10T19:42:18Z",
                                "fhr": [ 146.0, 146.0, 147.0, 147.0 ],
                                "qfhr": [ 2, 2, 2, 2 ],
                                "mhr": [ 74.0, 74.0, 74.0, 74.0 ],
                                "toco": [ 27.0, 27.0, 27.0, 27.0 ]
                            }
                        },
                        {
                            "ctgStop": {
                                "stopTime": "2016-10-10T19:43:18Z"
                            }
                        }
                    ]
                }
            }

  + Schema

            <!-- include(../../priv/static/json_schemas/create-continuous-ctg.json) -->

+ Response 201 (application/json)

  + Body

            {
              "status": "ok"
            }

+ Response 422 (application/json)

  + Body

            {
              "message": "Schema validation failed",
              "errors": [
                {
                  "resource": "continuous_ctg",
                  "field": "patientRef",
                  "code": "Required property was not present."
                },
                {
                  "resource": "continuous_ctg",
                  "field": "data",
                  "cause": "Required property data was not present."
                }
              ]
            }

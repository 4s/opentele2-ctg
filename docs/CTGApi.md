FORMAT:1A

# CTG API

The CTG API allows the transfer of CTG measurements to an external system for
visualization.

## Authentication

The CTG API uses two forms of authentication.

### Questionnaire Based CTG

Questionnaire based CTG are received by the OpenTele-citizen service and
forwarded to OpenTele-CTG. Authentication is based on a shared
secret/API-key. The API-key is supplied in the header of requests to
OpenTele-CTG, see below. See the documentation configuration for how to setup
the API-key.

### Continuous CTG

Continuous CTG is received directly from the patient app and requests are
authenticated with `Basic Authentication`. The OpenTele CTG service therefore
requires access to the OpenTele-Server database in order to verify patient
credentials. See the documentation configuration.

## Hypermedia

Resources can include a *links* property containing absolute URLs to related
resources.  This means that clients should not construct the URLs on their own,
thus making the maintenance of the URL structure easier.

      "links": {
        "self": "https://<host>",
        "schemas": "https://<host>/schemas",
        "isAlive": "https://<host>/isAlive",
        "ctgStatus": "https://<host>/ctg/status",
        "ctg": "https://<host>/ctg",
        "continuousCtg": "https://<host>/continuous_ctg",
        "continuousCtgStatus": "https://<host>/continuous_ctg/status",
        "apiDoc": "https://<host>/docs/ctg-api.html"
      }

The `<host>` in the example above will be replaced with the real value.

The link relation `self` is the URI pointing to the resource being represented.

## Schema

All data is sent and received as JSON. Data sent has the `content-type` header
set to `application/json`.

Blank fields are included in responses as null:

    "someEmptyField": null

However, currently there are no responses that can include a `null` value.

Timestamps are returned in the format:

    YYYY-MM-DDThh:mm:ss.sTZD

Likewise, the timestamps used in requests to the server also have to use the
ISO-8601 UTC date time format above, which may include a timezone offset or be
in Zulu time.

In each of the following resource descriptions, note for every example of a
request JSON body there is an
accompanying [JSON schema](http://json-schema.org/) format describing the
possible types of each property and whether each property is required or
optional in the request body.

The full list of JSON schemas associated with this service can be found at:
`https://<host>/schemas`.

## HTTP Verbs
The following HTTP verbs are used throughout the API:

Verb   |Description
-------|-------------------------------
GET    |Used for retrieving resources
POST   |Used for creating new resources

## Client Errors

The common
[HTTP Response Status Codes](http://en.wikipedia.org/wiki/List_of_HTTP_status_codes)
are used.  The actual status codes used are described in the documentation for
the specific resources.

# Resources

<!-- include(resources/json-schemas.md) -->

<!-- include(resources/entry-point.md) -->

<!-- include(resources/ctg.md) -->

<!-- include(resources/ctg-status.md) -->

<!-- include(resources/continuous-ctg.md) -->

<!-- include(resources/continuous-ctg-status.md) -->

# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :open_tele_ctg, OpenTeleCtg.Endpoint,
  url: [host: "localhost"],
  root: Path.dirname(__DIR__),
  secret_key_base: "/+BwUJbyI7AGvOUBfW9+gXJbe/pdcEUioTvOWJ89mTsIhfwtonwpRKZ2CnF0Ujmb",
  render_errors: [accepts: ~w(json)],
  pubsub: [name: OpenTeleCtg.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

# Configure phoenix generators
config :phoenix, :generators,
  migration: true,
  binary_id: false

#Configure ex_json_schema to know how to resolve schema references ($ref).
config :ex_json_schema,
  :remote_schema_resolver,
  fn url -> File.read!(Application.app_dir(:open_tele_ctg) <> "/priv/static/json_schemas/#{url}") |> Poison.decode! end

# Configure logger
config :logger,
  backends: [:console, {LoggerFileBackend, :audit_log}]

config :logger, :auditlog,
  path: "/var/log/ctg/audit.log",
  format: "$message\n",
  level: :info,
  metadata_filter: [audit: true]

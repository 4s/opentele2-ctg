use Mix.Config

# For development, we disable any cache and enable
# debugging and code reloading.
#
# The watchers configuration can be used to run external
# watchers to your application. For example, we use it
# with brunch.io to recompile .js and .css sources.
config :open_tele_ctg, OpenTeleCtg.Endpoint,
  http: [port: 4000],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: []

# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"

config :logger, level: :debug, truncate: :infinity



# Set a higher stacktrace during development.
# Do not configure such in production as keeping
# and calculating stacktraces is usually expensive.
config :phoenix, :stacktrace_depth, 20

# Configure your database
config :open_tele_ctg, OpenTeleCtg.Repo,
  adapter: Ecto.Adapters.MySQL, # Change to 'Tds.Ecto' when using MSSQL
  username: "opentele",
  password: "opentele",
  database: "opentele_ctg_dev",
  hostname: "localhost",
  pool_size: 10

# OpenTele Server DB
config :open_tele_ctg, OpenTeleCtg.OpenTeleServer,
  repo: OpenTeleCtg.OpenTeleServer.Repo

config :open_tele_ctg, OpenTeleCtg.OpenTeleServer.Repo,
  adapter: Ecto.Adapters.MySQL,
  username: "opentele",
  password: "opentele",
  database: "opentele",
  hostname: "localhost",
  pool_size: 10

# Milou config
config :open_tele_ctg, OpenTeleCtg.Milou,
  #milou_endpoint: "http://localhost:4567/Milou/",
  milou_discrete_endpoint: "http://localhost:5678/Milou/",
  milou_continuous_endpoint: "http://localhost:5678/Milou/",
  discrete_millis_between_retries: 60000,
  discrete_exporter: OpenTeleCtg.Milou.Discrete,
  continuous_millis_between_retries: 5000,
  continuous_exporter: OpenTeleCtg.Milou.Continuous,
  continuous_ctg_millis_before_reporting_dead: 30000

# Configure logger
config :logger, :audit_log,
  path: "./audit.log"

# API Key
config :open_tele_ctg, api_key: "fd83f35e-9208-45ce-bf47-581f38d32450"

use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :open_tele_ctg, OpenTeleCtg.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :debug

# Configure your database
config :open_tele_ctg, OpenTeleCtg.Repo,
  adapter: Ecto.Adapters.MySQL,
  username: "jenkins",
  password: "QMsXxNDudwVJ3Mja7ZYs",
  database: "opentele-ctg-jenkins",
  hostname: "opentele-group-sessions-jenkins.clltqqttx7ny.eu-west-1.rds.amazonaws.com",
  pool: Ecto.Adapters.SQL.Sandbox

# OpenTele Server DB
config :open_tele_ctg, OpenTeleCtg.OpenTeleServer,
  repo: OpenTeleCtg.Test.OpenTeleServerRepo # OpenTeleCtg.Test.OpenTeleServerRepo

# Required even though we use a mock repo
config :open_tele_ctg, OpenTeleCtg.OpenTeleServer.Repo,
  adapter: Ecto.Adapters.MySQL

# Test
config :junit_formatter,
  print_report_file: true

# Milou config
config :open_tele_ctg, OpenTeleCtg.Milou,
  milou_discrete_endpoint: "http://localhost:4567/Milou/",
  milou_continuous_endpoint: "http://localhost:4567/Milou/",
  discrete_millis_between_retries: 100,
  discrete_exporter: OpenTeleCtg.Test.DiscreteCtgExporter,
  continuous_millis_between_retries: 100,
  continuous_exporter: OpenTeleCtg.Test.ContinuousCtgExporter,
  continuous_ctg_millis_before_reporting_dead: 30000

# API Key
config :open_tele_ctg, api_key: "fd83f35e-9208-45ce-bf47-581f38d32450"

# OpenTeleCTG

## Design and architecture

In this section, we first describe the design and use case of the component,
followed by a description of the general architecture of the solution.

### Design and use case

OpenTele is based on answering questionnaires and taking
measurements. Questionnaires are created by clinicians, and the simplest example
could be a questionnaire consisting of a single CTG measurement "node".

The patients use an Android tablet with an installed App (The OpenTele Client)
which fetches questionnaires from the OpenTele server. The patient must then
complete the questionnaires setup by the clinician. The result of a completed
questionnaire is a series of measurement results, i.e. a CTG measurement
series. When filling out a questionnaire, the patient uses a Monica AN24 device
for measuring CTG. The AN24 device then sends measurement data to the Android
App via a Bluetooth protocol.

CTG data can also be streamed, as a continuous series of CTG events, to the
OpenTele server outside the context of a questionnaire.

Whether the data is sent as a questionnaire result or as a stream of data, the
OpenTele server can be configured to forwarding the received CTG data to the
Milou server via the Milou SOAP API for receiving CTG measurements.

### Architecture of the general solution

The general architecture of the above CTG solution is as follows:

![Architecture](../priv/images/architecture.png "Architecture")

1. The raw CTG data is transferred from the Monica AN24 device to the device
   integration layer of the OpenTele Android client on the tablet device.
2. If the CTG data is transferred as a continuous series of measurement events,
   the HTML5 layer on top of the device layer streams the received data directly
   to the OpenTele-CTG backend as batches of JSON formatted events.
3. If the CTG data is transferred as a complete measurement series, the received
   data is sent as part of a questionnaire result to the OpenTele-Server.
4. After the data has been received at the OpenTele-Server, a background job is
   in charge of sending unexported CTG measurement series to the OpenTele-CTG
   backend.
5. After the continuous CTG data has reached the OpenTele-CTG service, an
   exporter is created which is in charge of transforming the JSON events into
   SOAP messages and send them to the Medexa Milou backend for visualization.
6. Similar to the continuous CTG case, when a complete measurement series has
   reached OpenTele-CTG it is transformed into SOAP and given to a pool of
   export workers in charge of sending the data to the Medexa Milou backend.

### Internal Architecture of opentele-ctg

The architecture of the `opentele-ctg` service follows the normal structure of
a [Phoenix](https://github.com/phoenixframework/phoenix) project for organizing
API endpoints, models and plugs, and then has a dedicated `web/modules` folder
containing the modules for most of the business logic, i.e. exporting CTG
measurements and looking up patients in the opentele DB.

We split this section up in the following two subsections: Continuous CTG and
Discrete CTG, and for each of these go through the involved modules needed for
exporting data to Milou.

#### Continuous CTG

When streaming continuous CTG measurements via `opentele-ctg`, all measurement
event data is initially received by the `ContinuousCtgController`.

If the measurement events contain a `CtgStart` event, the `CtgStart` event and
`patientReference` is used to start a `Milou.Continuous` exporter process for
the continuous CTG streaming session, which in turn starts an `Exporter` process
and adds an entry to the `StatusRegistry`.

All `CtgSample` and `CtgSignal` events in the measurement series are passed
to - an already started - `Milou.Continuous` which puts the events on its
`Exporter`'s queue. The `Exporter` then routinely grabs all events in its queue,
converts the to SOAP XML via the `NewMessage` module and sends them to the Milou
backend via the `Soap` module.

If the measurement events contain a `CtgStop` event, the `CtgStop` event and
`patientReference` is used to stop the `Milou.Continuous` process associated
with the continuous CTG streaming session, where the `Exporter` is stopped and
the `patientReference` is unregistered in the `StatusRegistry`. When stopping
the `Exporter` it also sends a SOAP XML request to the Milou backend to inform
it that the continuous CTG session has stopped.

#### Discrete CTG

When receiving a whole CTG measurement series via `opentele-ctg`, the
measurement series is initially received by the `CtgController`.

The `CtgController` saves the whole measurement series to the database, marks
its export status as `initial`, and hands the CTG data to the `Milou.Discrete`
process.

The `Milou.Discrete` process in turn passes the CTG data onto the `CtgExport`
worker pool along with a description of the Milou endpoint it should be exported
to and finally a `millis_between_retries` option, in increase resilience against
an unresponsive Milou server.

At some point the `CtgExport` worker picks up the CTG series, converts it into a
SOAP XML message via the `ImportCtg` module and passes the SOAP XML message to
the `Soap` module, which finally sends the data to the Milou endpoint.

If the whole CTG measurement series is successfully exported to the Milou
backend, the corresponding CTG series saved in the database is marked as
`exported`, and if the export fails after 3 retries it is marked as `failed`.

## Installation and deployment

In this section, we first describe how to setup a local installation of the
OpenTele CTG project, followed by a guide on what to be aware of when
deploying.

### Installation (on OS/X)

#### Databases

As a prerequisite for setting up a local installation of OpenTele-CTG, a running
MySQL server is needed whose credentials should correspond to the ones specified
in `config/dev.exs`, see

    config :open_tele_ctg, OpenTeleCtg.Repo
      adapter: Ecto.Adapters.MySQL
      ...

The database is used to store CTG data transferred from the OpenTele-Server.

Furthermore, OpenTele-CTG also needs access to the OpenTele-Server database,
whose credentials are also specified in `config/dev.exs`, see

    config :open_tele_ctg, OpenTeleCtg.OpenTeleServer.Repo
      adapter: Ecto.Adapters.MySQL
      ...

The database is used to look up user credentials when authenticating requests
coming from the Android client.

#### Elixir

Install elixir with the command

    brew install elixir

Since `elixir` bundles a package manager, `hex`, and a build tool, `mix`,
nothing else should be needed in order to setup the phoenix project.

*Note:* While the project should build and run correctly with the newest Elixir
version, it was originally developed using Elixir 1.3 and Erlang 19. In order to
install and maintain older versions of Elixir and Erlang we recommend using
the [asdf](https://github.com/asdf-vm/asdf) version manager tool.

#### Setting up the Phoenix project

To start your OpenTele-CTG Phoenix app:

  * Install dependencies with `mix deps.get`,
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`,
  * Start Phoenix endpoint with `mix phoenix.server`

**Note:** The `mix ecto.setup` task will create and migrate the database and
commit a bit of seed data.

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

### Deployment considerations

As already mentioned in the previous section, the database connection is
configured in `config/dev.exs` (or `config/prod.exs` for production).

Other things to be aware of in the config file include:

- `config :open_tele_ctg, OpenTeleCtg.Endpoint` which configures the URL and
  port of the service endpoint,
- `config :logger` which configures logging and audit logging,
- `config :open_tele_ctg, OpenTeleCtg.Milou` which configures the exporters that
  send data to the Milou endpoint, and
- `config :open_tele_ctg, api_key: <api_key>` which configures the API key used
  to exchange data between the OpenTele-Server and OpenTele-CTG.

## Developers guide

### Developer API Documentation

The developer API documentation, describing the internal modules of the project,
is generated using [ExDoc](https://github.com/elixir-lang/ex_doc) and is
installed along with the other dependencies of the project.

Run the task

    mix doc.dev_api

to generate the developer API documentation, located at `doc/index.html`.

### REST API Documentation

#### The Format of the REST API Documentation

The REST API documentation, describing the externally visible endpoints of the
service, is written using the [API Blueprint](http://apiblueprint.org/)
format. Some examples can be found
[here](https://github.com/apiaryio/api-blueprint/tree/master/examples). The
documentation is generated using
[Aglio](https://github.com/danielgtaylor/aglio).

#### Installing Aglio

Aglio is used to generate the HTML version of the API documentation. Aglio is
installed using npm as described below:

    npm install -g aglio

The currently committed documentation was created using aglio v2.2.0 and node
v5.12.0.

#### Generating HMTL API Documentation

Type the following in a command prompt/terminal in the root folder of the
project

    mix doc.rest_api

this generates the HTML API docs, which can then be browsed by starting the
server

    mix phoenix.server

and visiting the
[`localhost:4000/docs/ctg-api.html`](http://localhost:4000/docs/ctg-api.html)
from your browser.

## Static code analysis

Type specs can be added to methods to document the expected input and output
types of a function, see
`/opentele-ctg/web/modules/milou/discrete/messages/import_ctg_message.ex` for a
simple example. For more information on type specs, refer to the
official
[getting started guide](http://elixir-lang.org/getting-started/typespecs-and-behaviours.html).

These typespecs do not confer any compile or runtime checks! They serve only as
documentation and to form the basis for the static code analysis tool: dialyzer.

The dialyzer tool can perform static code analysis.  First run `mix
dialyzer.plt`. This takes while but only has to be done when Elixir or Erlang
has been upgraded. To run the static analysis run `mix dialyzer`.

It does generate a lot of noise but also some helpful hints.

### An example

Consider the following methods

```
@spec build_milou_import_ctg_message(%Ctg{}) :: String.t
def build_milou_import_ctg_message(ctg) do

end
```

Running dialyzer will give the following warnings:

`Invalid type specification for function :build_milou_import_ctg_message/1. The
success typing is (_) -> 'nil'`

Lets fix that:

```
@spec build_milou_import_ctg_message(%Ctg{}) :: String.t
def build_milou_import_ctg_message(ctg) do
  "result!"
end
```

And the warning goes away. Lets try and call the
`build_milou_import_ctg_message` function with a String instead of a `Ctg`
struct.

```
def foo() do
  build_milou_import_ctg_message("")
end
```

And we get the following warning:

`The call :build_milou_import_ctg_message(<<_:24>>) breaks the contract (#{}) ->
'Elixir.String':t()`

## Linting

Use the `mix credo suggest` task to get linting hints for the Elixir code. For
more information look [here](https://github.com/rrrene/credo).

## Testing

The project tests are located in the `test` folder, in it:

* helper functions are located in `test/support`, and
* the remaining folders contain tests for the different aspects of the app.

The test are mostly integration-level tests.

Use the following task

    mix test

to run the test suite, or

    mix coveralls.html

to generate code coverage in an HTML format, which can be found in
`cover/excoveralls.html`.

No tests are performed directly against the Milou system nor a mock hereof. The
tests utilize a mock of the components responsible for performing the export of
CTGs to Milou. The mocks are setup via `config/test.exs`.

## Operations

Deploying the service on a server is very similar to deploying it locally. All
that is needed is:

- elixir and erlang installed,
- a reachable MySQL database for the CTG data to be stored,
- a reachable MySQL database for the OpenTele-Server data,
- a correctly configured `config/prod.exs`,
- an optional Milou endpoint,
- the server can be started with the command: `MIX_ENV=prod mix phoenix.server
  --no-deps-check`
- remember to monitor the log and auditlog files generated and saved locally by
  the server.

As the application is running on the Erlang VM using the OTP library, it should
be very resilient towards errors and rarely need manual intervention.

The service exposes an `isAlive` endpoint (link provided by the root resource)
that allows monitoring of the health of the service. The isAlive endpoint checks
that it is possible to connect to the database and responds with a success code
it is.

## Configuration

Configuration of the OpenTele-CTG service happens in the config files located in
`/config`. Most of the configuration can be left as it is but there are a few
areas require attention.

### Example configuration of database connections

OpenTele-CTG requires its own database:

    # Configure your database
    config :open_tele_ctg, OpenTeleCtg.Repo,
      adapter: Ecto.Adapters.MySQL, # Change to 'Tds.Ecto' when using MSSQL
      username: "opentele",
      password: "opentele",
      database: "opentele_ctg_dev",
      hostname: "localhost",
      pool_size: 10

OpenTele-CTG also requires access to the OpenTele-Sever database:

    config :open_tele_ctg, OpenTeleCtg.OpenTeleServer.Repo,
      adapter: Ecto.Adapters.MySQL,
      username: "opentele",
      password: "opentele",
      database: "opentele",
      hostname: "localhost",
      pool_size: 10

### Configure connection to Milou

The Medexa Milou system is used to visualise the CTG measurements. Besides the
possibility of configuring the Milou endpoint there is also the possibility of
fine tuning the timeout values when exporting. The `discrete_exporter` and
`continuous_exporter` offers the possibility of replacing the real exporters
with mocked versions when testing, see: `config/test.exs`.

    # Milou config
    config :open_tele_ctg, OpenTeleCtg.Milou,
      milou_endpoint: "http://195.67.117.70/Milou/",
      discrete_millis_between_retries: 60000,
      discrete_exporter: OpenTeleCtg.Milou.Discrete,
      continuous_millis_between_retries: 5000,
      continuous_exporter: OpenTeleCtg.Milou.Continuous

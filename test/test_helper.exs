ExUnit.start

Mix.Task.run "ecto.create", ~w(-r OpenTeleCtg.Repo --quiet)
Mix.Task.run "ecto.migrate", ~w(-r OpenTeleCtg.Repo --quiet)
Ecto.Adapters.SQL.begin_test_transaction(OpenTeleCtg.Repo)

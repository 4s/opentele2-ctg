defmodule OpenTeleCtg.CtgStatusControllerTest do
  use OpenTeleCtg.ConnCase, async: false

  setup do
    OpenTeleCtg.Test.DiscreteCtgExporter.start_link()
    :ok
  end

  test "Returns 404 when requesting the status of ctg measurement that does not exist", %{conn: conn} do
    conn = get_with_auth(conn, "/ctg/status?reference=some_id")
    json_response(conn, 404)
  end

  test "It should provide status for existing ctgs", %{conn: conn} do
    post_conn = post_with_auth(conn, ctg_path(conn, :create), valid_ctg("https://example.org/patient/13/measurements/23"))
    %{"links" => %{"status" => status_url}} = json_response(post_conn, 201)

    get_conn = get_with_auth(conn, status_url)

    %{ "statuses" => [
      %{ "status" => "initial",
         "links" => %{ "status" => ^status_url }
       }
    ],
       "links" => %{ "self" => ^status_url }
    } = json_response(get_conn, 200)

  end

  test "It can handle multiple references in query string", %{conn: conn} do
    post_conn = post_with_auth(conn, ctg_path(conn, :create), valid_ctg("https://example.org/patient/13/measurements/23"))
    %{"links" => %{"status" => _}} = json_response(post_conn, 201)

    post_conn = post_with_auth(conn, ctg_path(conn, :create), valid_ctg("https://example.org/patient/13/measurements/24"))

    %{"links" => %{"status" => _}} = json_response(post_conn, 201)

    get_conn = get_with_auth(conn, "/ctg/status?reference[]=https%3A%2F%2Fexample.org%2Fpatient%2F13%2Fmeasurements%2F23&reference[]=https%3A%2F%2Fexample.org%2Fpatient%2F13%2Fmeasurements%2F24")
    json_response(get_conn, 200)
  end

  defp valid_ctg(reference) do
    %{ctg: %{
         patientRef: %{
           uniqueId: "2512484916",
           firstName: "Nany Ann",
           lastName: "Doe",
           links: %{
             patient: "https://oth-devel.oth.io/oth/api/patients/17"
           }
         },
         data: %{
           reference: reference,
           startTime: Timex.today() |> Timex.format("{ISO:Extended:Z}") |> elem(1),
           endTime: Timex.today() |> Timex.shift(minutes: 31) |> Timex.format("{ISO:Extended:Z}") |> elem(1),
           deviceName: "Monia AN240213",
           uaDelay: 24,
           signals: [
             Timex.today() |> Timex.shift(seconds: 3)  |> Timex.format("{ISO:Extended:Z}") |> elem(1),
             Timex.today() |> Timex.shift(seconds: 10) |> Timex.format("{ISO:Extended:Z}") |> elem(1),
             Timex.today() |> Timex.shift(seconds: 21) |> Timex.format("{ISO:Extended:Z}") |> elem(1),
             Timex.today() |> Timex.shift(seconds: 34) |> Timex.format("{ISO:Extended:Z}") |> elem(1)
           ],
           fhr: [
             1.234,
             4.123,
             2.345
           ],
           qfhr: [
             0.1,
             0.2,
             0.4
           ],
           mhr: [
             32,
             54,
             66
           ],
           toco: [
             1,
             2,
             4.4
           ]
         }
      }
    }
  end
end

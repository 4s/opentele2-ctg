defmodule OpenTeleCtg.ContinuousCtgControllerTest do

  require Logger
  use OpenTeleCtg.ConnCase, async: false
  alias OpenTeleCtg.Milou.Continuous.StatusRegistry
  alias OpenTeleCtg.PatientReference

  setup do
    OpenTeleCtg.Test.ContinuousCtgExporter.start_link()
    :ok
  end

  test "It should register and trigger an export of the recieved CTG event", %{conn: conn} do
    post_conn = post_with_auth(conn, continuous_ctg_path(conn, :create), valid_continuous_ctg_events)
    response = json_response(post_conn, 201)

    assert response
  end

  test "It should return error if sending samples and signals before start", %{conn: conn} do
    post_conn = post_with_auth(conn, continuous_ctg_path(conn, :create), valid_continuous_ctg_events_missing_start)
    response = json_response(post_conn, 500)

    assert response["message"] == "Internal server error"
    error0 = Enum.fetch!(response["errors"], 0)
    assert String.contains?(error0["code"], "Could not find exporter")
    assert error0["resource"] == "continuous_ctg"
    assert error0["field"] == "continuous_ctg.data"
  end

  test "It should not allow unknown ctg event type", %{conn: conn} do
    post_conn = post_with_auth(conn, continuous_ctg_path(conn, :create), invalid_continuous_ctg_events)
    response = json_response(post_conn, 422)

    error0 = Enum.fetch!(response["errors"], 0)
    assert String.contains?(error0["code"], "Expected any of the schemata to match but none did")
    assert error0["resource"] == "continuous_ctg"
    assert error0["field"] == "continuousCtgdata1"
  end

  test "It should register the CTG in the status registry", %{conn: conn} do
    post_conn = post_with_auth(conn, continuous_ctg_path(conn, :create), start_event)
    response = json_response(post_conn, 201)

    {:ok, :low, _timestamp} = StatusRegistry.poll_status(%PatientReference{ unique_id: "2512484916"})
  end

  test "It should refresh the registration when a sample is received", %{conn: conn} do
    post_conn1 = post_with_auth(conn, continuous_ctg_path(conn, :create), start_event)
    _response = json_response(post_conn1, 201)
    {:ok, :low, initial_timestamp} = StatusRegistry.poll_status(%PatientReference{ unique_id: "2512484916"})

    post_conn2 = post_with_auth(conn, continuous_ctg_path(conn, :create), sample_event)
    _response = json_response(post_conn2, 201)
    {:ok, :low, updated_timestamp} = StatusRegistry.poll_status(%PatientReference{ unique_id: "2512484916"})

    assert Timex.after?(updated_timestamp, initial_timestamp)
  end

  test "It should refresh the registration when a signal is received", %{conn: conn} do
    post_conn1 = post_with_auth(conn, continuous_ctg_path(conn, :create), start_event)
    _response = json_response(post_conn1, 201)
    {:ok, :low, initial_timestamp} = StatusRegistry.poll_status(%PatientReference{ unique_id: "2512484916"})

    post_conn2 = post_with_auth(conn, continuous_ctg_path(conn, :create), signal_event)
    _response = json_response(post_conn2, 201)
    {:ok, :low, updated_timestamp} = StatusRegistry.poll_status(%PatientReference{ unique_id: "2512484916"})

    assert Timex.after?(updated_timestamp, initial_timestamp)
  end

  test "It should remove the registration when a stop message is received", %{conn: conn} do
    post_conn = post_with_auth(conn, continuous_ctg_path(conn, :create), valid_continuous_ctg_events)
    response = json_response(post_conn, 201)

    { :ok, :none } = StatusRegistry.poll_status(%PatientReference{ unique_id: "2512484916"})
  end

  defp valid_continuous_ctg_events_missing_start do
    ctg_events = valid_continuous_ctg_events
    cont_ctg = ctg_events.continuousCtg
    cont_ctg = %{ cont_ctg | data: tl(cont_ctg.data) }

    %{ ctg_events | continuousCtg: cont_ctg }
  end

  defp valid_continuous_ctg_events do
    %{
      continuousCtg: %{

        patientRef: %{
          uniqueId: "2512484916",
          firstName: "Nancy Ann",
          lastName: "Doe"
        },

        data: [

          %{
            ctgStart: %{
              deviceName: "AN24",
              uaDelay: 5,
              uaSensitivity: "low",
              registrationIdentifier: "1b62c080-b12e-4654-98f7-0f31d9c62e77"
            }
          },

          %{
            ctgSample: %{
              fhr: [ 1.234, 4.123, 2.345 ],
              qfhr: [ 0.1, 0.2, 0.4 ],
              mhr: [ 0.32, 0.54, 0.66 ],
              toco: [ 0.1, 0.2, 4.4 ],
              readTime: Timex.now() |> Timex.shift(minutes: 12) |> Timex.format("{ISO:Extended:Z}") |> elem(1),
              sampleCount: 1
            }
          },

          %{
            ctgSignal: %{
              signalTime: Timex.now() |> Timex.shift(minutes: 31) |> Timex.format("{ISO:Extended:Z}") |> elem(1)
            }
          },

          %{
            ctgSample: %{
              fhr: [ 5.12, 3.55, 4.31 ],
              qfhr: [ 0.3, 0.4, 0.7 ],
              mhr: [ 0.82, 0.27, 0.54 ],
              toco: [ 0.2, 0.4, 5.4 ],
              readTime: Timex.now() |> Timex.format("{ISO:Extended:Z}") |> elem(1),
              sampleCount: 2
            }
          },

          %{
            ctgStop: %{
              stopTime: Timex.now() |> Timex.shift(hours: 1) |> Timex.format("{ISO:Extended:Z}") |> elem(1)
            }
          }

        ]
      }
    }
  end

  defp invalid_continuous_ctg_events do
    %{
      continuousCtg: %{

        patientRef: %{
          uniqueId: "2512484916",
          firstName: "Nancy Ann",
          lastName: "Doe"
        },

        data: [

          %{
            ctgStart: %{
              deviceName: "AN24",
              uaDelay: 5,
              uaSensitivity: "high",
              registrationIdentifier: "1b62c080-b12e-4654-98f7-0f31d9c62e77",
              startTime: Timex.now() |> Timex.format("{ISO:Extended:Z}") |> elem(1),
            }
          },

          %{
            ctgFoo: %{
              bar: 42
            }
          },

          %{
            ctgStop: %{
              stopTime: Timex.now() |> Timex.shift(hours: 1) |> Timex.format("{ISO:Extended:Z}") |> elem(1)
            }
          }

        ]
      }
    }
  end

  defp start_event do
    %{
      continuousCtg: %{

        patientRef: %{
          uniqueId: "2512484916",
          firstName: "Nancy Ann",
          lastName: "Doe"
        },

        data: [

          %{
            ctgStart: %{
              deviceName: "AN24",
              uaDelay: 5,
              uaSensitivity: "low",
              registrationIdentifier: "1b62c080-b12e-4654-98f7-0f31d9c62e77"
            }
          }
        ]
      }
    }
  end

  defp sample_event do
    %{
      continuousCtg: %{

        patientRef: %{
          uniqueId: "2512484916",
          firstName: "Nancy Ann",
          lastName: "Doe"
        },

        data: [

          %{
            ctgSample: %{
              fhr: [ 5.12, 3.55, 4.31 ],
              qfhr: [ 0.3, 0.4, 0.7 ],
              mhr: [ 0.82, 0.27, 0.54 ],
              toco: [ 0.2, 0.4, 5.4 ],
              readTime: Timex.now() |> Timex.format("{ISO:Extended:Z}") |> elem(1),
              sampleCount: 2
            }
          }
        ]
      }
    }
  end

  defp signal_event do
    %{
      continuousCtg: %{

        patientRef: %{
          uniqueId: "2512484916",
          firstName: "Nancy Ann",
          lastName: "Doe"
        },

        data: [

          %{
            ctgStart: %{
              deviceName: "AN24",
              uaDelay: 5,
              uaSensitivity: "low",
              registrationIdentifier: "1b62c080-b12e-4654-98f7-0f31d9c62e77"
            }
          }
        ]
      }
    }
  end

end

defmodule OpenTeleCtg.ContinuousCtgStatusControllerTest do

  require Logger
  use OpenTeleCtg.ConnCase, async: false
  alias OpenTeleCtg.Milou.Continuous.StatusRegistry
  alias OpenTeleCtg.PatientReference

  setup do
    OpenTeleCtg.Test.ContinuousCtgExporter.start_link()
    :ok
  end

  test "It should report when no continuous CTG is running for patient", %{conn: conn} do
    post_conn = post_with_auth(conn,
      continuous_ctg_path(conn, :create), start_event("low"))
    _response = json_response(post_conn, 201)
    post_conn = get_with_auth(conn,
      continuous_ctg_status_path(conn, :status, "2512484916"))
    %{ "status" => "active", "last_event" => _, "sensitivity" => _ } =
      json_response(post_conn, 200)
  end

  test "It should correctly report low when low sensitivity was selected", %{conn: conn} do
    post_conn = post_with_auth(conn,
      continuous_ctg_path(conn, :create), start_event("low"))
    _response = json_response(post_conn, 201)
    post_conn = get_with_auth(conn,
      continuous_ctg_status_path(conn, :status, "2512484916"))
    %{ "sensitivity" => "low", "last_event" => _, "status" => _ } =
      json_response(post_conn, 200)
  end

  test "It should correctly report high when high sensitivity was selected", %{conn: conn} do
    post_conn = post_with_auth(conn,
      continuous_ctg_path(conn, :create), start_event("high"))
    _response = json_response(post_conn, 201)
    post_conn = get_with_auth(conn,
      continuous_ctg_status_path(conn, :status, "2512484916"))
    %{ "sensitivity" => "high", "last_event" => _, "status" => _ } =
      json_response(post_conn, 200)
  end

  test "It should detect when no events have been received within the timeout period", %{conn: conn} do
    original_config = Application.get_env(:open_tele_ctg, OpenTeleCtg.Milou)
    updated_config = Keyword.update!(original_config,
      :continuous_ctg_millis_before_reporting_dead, fn _ -> 0 end)
    Application.put_env(:open_tele_ctg, OpenTeleCtg.Milou, updated_config)

    post_conn = post_with_auth(conn, continuous_ctg_path(conn, :create),
      start_event("high"))
    _response = json_response(post_conn, 201)

    post_conn = post_with_auth(conn, continuous_ctg_path(conn, :create),
      sample_event())
    _response = json_response(post_conn, 201)

    post_conn = get_with_auth(conn,
      continuous_ctg_status_path(conn, :status, "2512484916"))

    %{"last_event" => _, "status" => "timeout"} = json_response(post_conn, 200)

    Application.put_env(:open_tele_ctg, OpenTeleCtg.Milou, original_config)
  end

  defp patient_reference do
    %{
      patientRef: %{
      uniqueId: "2512484916",
      firstName: "Nancy Ann",
      lastName: "Doe"
      }
    }
  end

  defp start_event(sensitivity) do
    %{
      continuousCtg: %{

        patientRef: %{
          uniqueId: "2512484916",
          firstName: "Nancy Ann",
          lastName: "Doe"
        },

        data: [

          %{
            ctgStart: %{
              deviceName: "AN24",
              uaDelay: 5,
              uaSensitivity: "#{sensitivity}",
              registrationIdentifier: "1b62c080-b12e-4654-98f7-0f31d9c62e77"
            }
          }
        ]
      }
    }
  end

  defp sample_event do
    %{
      continuousCtg: %{

        patientRef: %{
          uniqueId: "2512484916",
          firstName: "Nancy Ann",
          lastName: "Doe"
        },

        data: [

          %{
            ctgSample: %{
              fhr: [ 5.12, 3.55, 4.31 ],
              qfhr: [ 0.3, 0.4, 0.7 ],
              mhr: [ 0.82, 0.27, 0.54 ],
              toco: [ 0.2, 0.4, 5.4 ],
              readTime: Timex.now() |> Timex.format("{ISO:Extended:Z}") |> elem(1),
              sampleCount: 2
            }
          }
        ]
      }
    }
  end

  defp signal_event do
    %{
      continuousCtg: %{

        patientRef: %{
          uniqueId: "2512484916",
          firstName: "Nancy Ann",
          lastName: "Doe"
        },

        data: [

          %{
            ctgStart: %{
              deviceName: "AN24",
              uaDelay: 5,
              uaSensitivity: "low",
              registrationIdentifier: "1b62c080-b12e-4654-98f7-0f31d9c62e77"
            }
          }
        ]
      }
    }
  end

end

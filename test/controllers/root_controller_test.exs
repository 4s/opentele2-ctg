defmodule OpenTeleCtg.RootControllerTest do
  use OpenTeleCtg.ConnCase

  test "It should return link to self", %{conn: conn} do
    conn = get conn, "/"
    %{ "links" => %{ "self" => self_link } } = json_response(conn, 200)

    conn = get conn, self_link
    json_response(conn, 200)
  end

  test "It should return link to schemas", %{conn: conn} do
    conn = get conn, "/"
    %{ "links" => %{ "schemas" => schemas_link } } = json_response(conn, 200)

    conn = get conn, schemas_link
    json_response(conn, 200)
  end

  test "It should return link to ctg resource", %{conn: conn} do
    conn = get conn, "/"
    %{ "links" => %{ "ctg" => _ } } = json_response(conn, 200)
  end

  test "It should return link to ctg status resource", %{conn: conn} do
    conn = get conn, "/"
    %{ "links" => %{ "ctgStatus" => _ } } = json_response(conn, 200)
  end

  test "It should return link to api documentation", %{conn: conn} do
    conn = get conn, "/"
    %{ "links" => %{ "apiDoc" => api_doc } } = json_response(conn, 200)

    conn = get conn, api_doc
    html_response(conn, 200)
  end

  test "It should return link to isAlive", %{conn: conn} do
    conn = get conn, "/"
    %{ "links" => %{ "isAlive" => is_alive } } = json_response(conn, 200)

    conn = get conn, is_alive
    response(conn, 200)
  end

end

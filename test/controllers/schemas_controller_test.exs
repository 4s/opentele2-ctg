defmodule OpenTeleCtg.SchemasControllerTest do
  use OpenTeleCtg.ConnCase

  test "Schemas response contains self link", %{conn: conn} do
    conn = get conn, "/schemas"
    %{"self" => _} = json_response(conn, 200)
  end

  test "Schemas response contains link to create-ctg schema", %{conn: conn} do
    conn = get conn, "/schemas"
    %{"create-ctg" => schema_url} = json_response(conn, 200)

    conn = get conn, schema_url
    json_response(conn, 200)
  end

  test "Shemas response contains link to patient-definitions schema", %{conn: conn} do
    conn = get conn, "/schemas"
    %{"patient-definitions" => schema_url} = json_response(conn, 200)

    conn = get conn, schema_url
    json_response(conn, 200)
  end
end

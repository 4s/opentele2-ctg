defmodule OpenTeleCtg.Milou.Continuous.ExporterTest do
  use ExUnit.Case, async: false

  require Logger
  alias OpenTeleCtg.Milou.Continuous.Exporter
  alias OpenTeleCtg.{CtgSample, CtgSignal, CtgStart, CtgStop, PatientReference}

  setup do
    Application.ensure_all_started(:bypass)
    bypass = Bypass.open

    original_config = Application.get_env(:open_tele_ctg, OpenTeleCtg.Milou)
    updated_config = Keyword.update!(original_config, :milou_continuous_endpoint, fn _ ->  "http://localhost:#{bypass.port}/" end)
    Application.put_env(:open_tele_ctg, OpenTeleCtg.Milou, updated_config)

    {:ok, exporter} = Exporter.start_link
    {:ok, bypass: bypass, exporter: exporter}
  end

  test "can add events to queue and remove them again", %{exporter: exporter} do
    assert [] = Exporter.dequeue(exporter)

    Exporter.enqueue(exporter, [
          %CtgSample{fhr: [], mhr: [], count: 1, qfhr: [],
                     toco: [], read_time: Timex.now()},
          %CtgSignal{signal_time: Timex.now() |> Timex.shift(minutes: 5)}
        ])

    assert [sample, signal] = Exporter.dequeue(exporter)
    assert sample.__struct__ == CtgSample
    assert signal.__struct__ == CtgSignal

    assert [] = Exporter.dequeue(exporter)
  end

  test "can stop the exporter", %{exporter: exporter} do
    assert nil == Exporter.get_stop_event(exporter)

    ctg_stop = %CtgStop{stop_time: Timex.now}
    Exporter.stop(exporter, ctg_stop)

    assert ctg_stop == Exporter.get_stop_event(exporter)
  end

  test "can start the exporter", %{exporter: exporter} do
    patient_reference = %PatientReference{unique_id: "2512484916",
                                          first_name: "Nancy Ann",
                                          last_name: "Doe"}

    ctg_start = %CtgStart{device_name: "AN24",
                          ua_delay: 4,
                          registration_id: "1b62c080-b12e-4654-98f7-0f31d9c62e77"}

    assert :ok == Exporter.start(exporter, patient_reference, ctg_start)
  end

  test "can send pending events", %{exporter: exporter, bypass: bypass} do
    reply_with_status(bypass, 200, "NewMessage")

    patient_reference = %PatientReference{unique_id: "2512484916",
                                          first_name: "Nancy Ann",
                                          last_name: "Doe"}

    meta = %{device_name: "AN24",
             registration_id: "1b62c080-b12e-4654-98f7-0f31d9c62e77",
             ua_delay: 4}

    ctg_events = [

      %CtgSample{fhr: [ 1.234, 4.123, 2.345 ],
                 qfhr: [ 0.1, 0.2, 0.4 ],
                 mhr: [ 0.32, 0.54, 0.66 ],
                 toco: [ 0.1, 0.2, 4.4 ],
                 read_time: Timex.now(),
                 count: 1},

      %CtgSignal{signal_time: Timex.now() |> Timex.shift(minutes: 7)},

      %CtgSample{fhr: [ 5.12, 3.55, 4.31 ],
                 qfhr: [ 0.3, 0.4, 0.7 ],
                 mhr: [ 0.82, 0.27, 0.54 ],
                 toco: [ 0.2, 0.4, 5.4 ],
                 read_time: Timex.now() |> Timex.shift(minutes: 10),
                 count: 2}

    ]

    Exporter.enqueue(exporter, ctg_events)

    ctg_stop = %CtgStop{stop_time: Timex.now()}
    Exporter.stop(exporter, ctg_stop)

    endpoint = Application.get_env(:open_tele_ctg, OpenTeleCtg.Milou)[:milou_continuous_endpoint]
    millis_between_retries = Application.get_env(:open_tele_ctg, OpenTeleCtg.Milou)[:millis_between_retries]
    export_config = %{endpoint: endpoint,
                      millis_between_retries: millis_between_retries}

    Exporter.send_pending_events(exporter, patient_reference, meta, export_config, Timex.now())

    assert not (Process.alive? exporter)
  end

  defp reply_with_status(bypass, status_code, type) do
    Bypass.expect bypass, fn conn ->
      assert "POST" == conn.method

      Plug.Conn.resp(conn, status_code, ~s(
            <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
          <s:Body>
          <#{type}Response xmlns="http://tempuri.org/"/>
          </s:Body>
          </s:Envelope>))
    end
  end

end

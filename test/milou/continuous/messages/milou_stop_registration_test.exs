defmodule OpenTeleCtg.Milou.Continuous.StopRegistrationTest do
  use ExUnit.Case, async: true

  require Logger
  alias OpenTeleCtg.Milou.Continuous.StopRegistration
  alias OpenTeleCtg.CtgStop

  test "It can generate a XML message" do
    xml = StopRegistration.message valid_ctg_stop, valid_meta
    # Logger.debug to_string("SOAP (new message):\n#{xml}")
    assert xml
  end

  defp valid_meta do
    %{device_name: "ACME Model 1",
      registration_id: "ABCD-1234",
      ua_delay: 4}
  end

  def valid_ctg_stop do
    %CtgStop{stop_time: Timex.now()}
  end

end

defmodule OpenTeleCtg.Milou.Continuous.NewMessageTest do
  use ExUnit.Case, async: true

  require Logger
  alias OpenTeleCtg.Milou.Continuous.NewMessage
  alias OpenTeleCtg.{PatientReference, CtgSample, CtgSignal}

  test "It can generate a XML message" do
    xml = NewMessage.message(valid_ctg_events, valid_patient_reference, valid_meta)
    # Logger.debug to_string("SOAP (new message):\n#{xml}")
    assert xml
  end

  defp valid_patient_reference do
    %PatientReference{unique_id: "2512484916",
                      first_name: "Nancy Ann",
                      last_name: "Berggren"}
  end

  defp valid_meta do
    %{device_name: "ACME Model 1",
      registration_id: "ABCD-1234",
      ua_delay: 4}
  end

  def valid_ctg_events do
    [
      %CtgSample{fhr: [ 140.75, 141.00, 142.25 ],
                 mhr: [ 71.75, 74.75, 72.5 ],
                 count: 42,
                 qfhr: [ 2, 2, 2 ],
                 toco: [ 27.0, 24.0, 26.0 ],
                 read_time: Timex.now() |> Timex.shift(minutes: 12)},
      %CtgSignal{signal_time: Timex.now() |> Timex.shift(minutes: 31)},
      %CtgSignal{signal_time: Timex.now() |> Timex.shift(minutes: 18)},
      %CtgSample{fhr: [ 144.5, 142.0, 143.0 ],
                 mhr: [ 71.75, 74.75, 72.5 ],
                 count: 41,
                 qfhr: [ 2, 2, 2 ],
                 toco: [ 27.0, 24.0, 26.0 ],
                 read_time: Timex.now()}
    ]
  end

end

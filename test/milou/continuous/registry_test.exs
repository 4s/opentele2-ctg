defmodule OpenTeleCtg.Milou.Continuous.RegistryTest do

  use ExUnit.Case, async: true
  require Logger
  alias OpenTeleCtg.Milou.Continuous.{Registry,Exporter}
  alias OpenTeleCtg.{PatientReference}

  setup context do
    {:ok, _} = Registry.start_link(context.test)
    {:ok, registry: context.test}
  end

  test "spawns exporter", %{registry: registry} do
    Logger.debug("Registry: #{inspect registry}")

    assert Registry.lookup(registry, valid_patient_reference) == :error

    {:ok, created_exporter} = Registry.create(registry, valid_patient_reference)
    Logger.debug "PID: #{inspect created_exporter}"

    {:ok, fetched_exporter} = Registry.lookup(registry, valid_patient_reference)
    Logger.debug "PID: #{inspect fetched_exporter}"

    Exporter.enqueue(fetched_exporter, [1,2,3])
    assert Exporter.dequeue(fetched_exporter) == [1,2,3]
  end

  test "removes exporter on exit", %{registry: registry} do
    Registry.create(registry, valid_patient_reference)
    {:ok, exporter} = Registry.lookup(registry, valid_patient_reference)
    Agent.stop(exporter)

    # Do a call to ensure the registry processed the DOWN message
    assert Registry.lookup(registry, valid_patient_reference) == :error
  end

  test "removes exporter on crash", %{registry: registry} do
    Registry.create(registry, valid_patient_reference)
    {:ok, exporter} = Registry.lookup(registry, valid_patient_reference)

    # Stop the bucket with non-normal reason
    Process.exit(exporter, :shutdown)

    # Wait until the bucket is dead
    ref = Process.monitor(exporter)
    assert_receive {:DOWN, ^ref, _, _, _}

    # Do a call to ensure the registry processed the DOWN message
    assert Registry.lookup(registry, valid_patient_reference) == :error
  end

  def valid_patient_reference do
    %PatientReference{unique_id: "2512484916",
                      first_name: "Nancy Ann",
                      last_name: "Doe"}
  end

end

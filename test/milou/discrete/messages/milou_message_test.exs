defmodule OpenTeleCtg.Milou.Discrete.Message.ImportCtgTest do
  use ExUnit.Case, async: true
  import OpenTeleCtg.TestHelpers

  alias OpenTeleCtg.Milou.Discrete.Message.ImportCtg

  test "It can generate a xml message" do
    create_ctg_measurement()
    |> ImportCtg.message()
  end
end

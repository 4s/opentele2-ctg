defmodule OpenTeleCtg.Milou.Discrete.ImportCtgTest do
  use OpenTeleCtg.ModelCase, async: false

  alias OpenTeleCtg.Milou.Discrete
  alias OpenTeleCtg.{Repo, Ctg}

  setup do
   Application.ensure_all_started(:bypass)
   bypass = Bypass.open
   original_config = Application.get_env(:open_tele_ctg, OpenTeleCtg.Milou)
   updated_config = Keyword.update!(original_config, :milou_discrete_endpoint, fn _ ->  "http://localhost:#{bypass.port}/" end)
   Application.put_env(:open_tele_ctg, OpenTeleCtg.Milou, updated_config)

   Discrete.start_link()

   ctg = create_ctg_measurement()
   {:ok, bypass: bypass, ctg: ctg}
 end

  test "Does export messages to Milou", %{bypass: bypass, ctg: ctg} do
    reply_with_status bypass, 200

    Discrete.export(ctg: ctg)

    # The call between Milou and the export worker is async,
    # so we have to sleep for a bit otherwise the application is closed before the worker starts working
    :timer.sleep(100)
  end

  test "Does update status when CTG is exported" , %{bypass: bypass, ctg: ctg} do
    reply_with_status bypass, 200

    Discrete.export(ctg: ctg)
    :timer.sleep(200)

    [ctg] = Repo.all(Ctg)

    assert ctg.status == "exported"
  end

  test "Does update status when CTG export fails" , %{bypass: bypass, ctg: ctg} do
    reply_with_status bypass, 500
    Discrete.export(ctg: ctg)
    :timer.sleep(500)

    [ctg] = Repo.all(Ctg)

    assert ctg.status == "failed"
  end

  test "Does pick up pending exports",  %{bypass: bypass, ctg: _ctg} do
    reply_with_status bypass, 200
    create_ctg_measurement("id1", "initial")
    create_ctg_measurement("id1", "initial")
    create_ctg_measurement("id1", "initial")
    create_ctg_measurement("id1", "initial")
    create_ctg_measurement("id1", "failed")


    Discrete.pick_up_pending_exports()

    :timer.sleep(300)
    ctgs = Repo.all(Ctg)

    refute Enum.any?(ctgs, fn(ctg) -> ctg.status == "initial" end)
    assert Enum.count(ctgs, fn(ctg) -> ctg.status == "exported" end) == 5 #Four created in this function, one from the setup method
  end

  defp reply_with_status(bypass, status_code) do
    Bypass.expect bypass, fn conn ->
      assert "POST" == conn.method

      Plug.Conn.resp(conn, status_code, ~s(
      <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
        <s:Body>
          <ImportCtgResponse xmlns="http://tempuri.org/"/>
        </s:Body>
      </s:Envelope>))
    end
  end
end

defmodule OpenTeleCtg.ErrorViewTest do
  use OpenTeleCtg.ConnCase, async: true

  import Phoenix.View

  test "renders 404.json" do
    assert %{message: "Not found",
             errors: [%{code: "not found",
                        field: "id",
                        resource: "some_resource"}]} ==
      render(OpenTeleCtg.ErrorView, "404.json", resource: "some_resource")
  end

  test "render 500.json" do
    assert %{message: "Internal server error"} ==
      render(OpenTeleCtg.ErrorView, "500.json", %{})
  end

  test "render any other" do
    assert %{message: "Internal server error"} ==
      render(OpenTeleCtg.ErrorView, "505.json", %{})
  end
end

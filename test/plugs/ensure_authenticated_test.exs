defmodule OpenTeleCtg.EnsureAuthenticatedTest do

  use OpenTeleCtg.ConnCase
  alias OpenTeleCtg.EnsureAuthenticated

  test "halts request pipeline and return status 401 if not authenticated" do
    conn = build_conn()
    |> EnsureAuthenticated.call(%{})

    assert conn.halted
    assert conn.status == 401
  end

  test "continues request pipeline if authenticated" do
    conn = build_conn()
    |> assign(:authenticated, true)
    |> EnsureAuthenticated.call(%{})

    assert conn.halted == false
    assert conn.status == nil
  end
end

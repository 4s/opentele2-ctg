defmodule OpenTeleCtg.Test.ContinuousCtgExporter do
  @moduledoc """
  Test helper module for asserting that a continuous CTG exporter has been called.
  """

  def start_link do
    state = %{started: false, samples_and_signals: [], stopped: false}
    Agent.start_link(fn -> state end, name: __MODULE__)
  end

  def start(patient_reference, ctg_start) do
    Agent.update(__MODULE__, fn state ->
      %{ state | started: true }
    end)
  end

  def has_started?() do
    Agent.get(__MODULE__, fn state -> state.started end)
  end

  def export(patient_reference, ctg_samples_and_signals) do
    if has_started? do
      Agent.update(__MODULE__, fn state ->
        new_samples_and_signals = state.samples_and_signals ++ ctg_samples_and_signals
        %{ state | samples_and_signals: new_samples_and_signals }
      end)
    else
      {:error, :not_found}
    end
  end

  def has_exported?(reference) do
    Agent.get(__MODULE__, fn state ->
      not Enum.empty? state.samples_and_signals
    end)
  end

  def stop(patient_reference, ctg_stop) do
    Agent.update(__MODULE__, fn state ->
      %{ state | started: true }
    end)
  end

  def has_stopped?() do
    Agent.get(__MODULE__, fn state -> state.stopped end)
  end

end

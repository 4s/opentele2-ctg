defmodule OpenTeleCtg.Test.OpenTeleServerRepo do
  @moduledoc """
  Test helper module for mocking an opentele server database.
  """

  require Logger
  alias OpenTeleCtg.OpenTeleServer.User

  def start_link do
    Agent.start_link(fn -> [] end, name: __MODULE__)
  end

  def get_by(user, keyword_list) do
    Logger.debug "OpenTeleCtg.Test.Soap::get_by called"
    %User{username: "Nancy Ann", password: "not a real password"}
  end

  def all(query) do
    Logger.debug "OpenTeleCtg.Test.Soap::all called"
    ["ROLE_REALTIME_CTG_SAVE"]
  end

end

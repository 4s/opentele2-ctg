defmodule OpenTeleCtg.Test.DiscreteCtgExporter do
  @moduledoc """
  Test helper module for asserting that a discrete CTG exporter has been called.
  """

  def start_link do
    Agent.start_link(fn -> [] end, name: __MODULE__)
  end

  def export(ctg: ctg) do
    Agent.update(__MODULE__, &(&1 ++ [ctg]))
  end

  def was_exported?(reference) do
    Agent.get(__MODULE__, fn(exported_ctgs) ->
      Enum.any? exported_ctgs, fn(x) -> x.external_id == reference end
    end)
  end

  def pick_up_pending_exports() do

  end

end

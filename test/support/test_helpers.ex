defmodule OpenTeleCtg.TestHelpers do
  alias OpenTeleCtg.{Repo, Ctg, Sample, Signal}

  # Requests

  def get_with_auth(conn, path) do
    conn
    |> Plug.Conn.put_req_header("authorization", "Basic bmFuY3lhbm46YWJjZDEyMzQ=")
    |> Phoenix.ConnTest.dispatch(OpenTeleCtg.Endpoint, "GET", path, nil)
  end

  def post_with_auth(conn, path, body) do
    conn
    |> Plug.Conn.put_req_header("authorization", "Basic bmFuY3lhbm46YWJjZDEyMzQ=")
    |> Phoenix.ConnTest.dispatch(OpenTeleCtg.Endpoint, "POST", path, body)
  end

  def get_with_api_key(conn, path) do
    conn
    |> Plug.Conn.put_req_header("authorization", "API key ZmQ4M2YzNWUtOTIwOC00NWNlLWJmNDctNTgxZjM4ZDMyNDUw")
    |> Phoenix.ConnTest.dispatch(OpenTeleCtg.Endpoint, "GET", path, nil)
  end

  def post_with_api_key(conn, path, body) do
    conn
    |> Plug.Conn.put_req_header("authorization", "API key ZmQ4M2YzNWUtOTIwOC00NWNlLWJmNDctNTgxZjM4ZDMyNDUw")
    |> Phoenix.ConnTest.dispatch(OpenTeleCtg.Endpoint, "POST", path, body)
  end

  def create_ctg_measurement(id \\ "some-id-sdfkg!?", status \\ "initial") do
    ctg = Repo.insert!(%OpenTeleCtg.Ctg{
      device_name: "Monia AN240213",
      end_time: "2016-05-21T20:17:31.401Z",
      external_id: id,
      patient_external_id: "2512484916",
      patient_first_name: "Nany Ann",
      patient_last_name: "Doe",
      #patient_link: "https://oth-devel.oth.io/oth/api/patients/17",
      start_time: "2016-05-21T19:46:31.400Z",
      status: status,
      ua_delay: 24
    })

    1..100
    |> Enum.each(fn(x) ->
      sample = Ecto.build_assoc(ctg, :samples, %{sequence_number: x, fhr: "#{x}", qfhr: "#{x}", mhr: "#{x}", toco: "#{x}"})
      Repo.insert!(sample)
    end)

    1..10
    |> Enum.each(fn(x) ->
      signal = Ecto.build_assoc(ctg, :signals, when: "at some unspecified time: x")
      Repo.insert!(signal)
    end)

    ctg
  end
end

defmodule Mix.Tasks.Opentelectg.Gen.ApiDoc do
  @moduledoc ~S"""
  Generates the REST API documentation.

  The script can be run with the following mix project command.

      $ mix doc.rest_api

  after which the REST API documentation can be found at
  `https://<host>/docs/ctg-api.html`.
  """

  @doc ~S"""
  Runs the API generating script.
  """
  @spec run(any) :: :ok
  def run(_) do
    source_location = "docs/CTGApi.md"
    output_destination = "priv/static/docs/ctg-api.html"

    System.cmd("mkdir", ["-p", "priv/static/docs"])

    aglio_result = System.cmd("aglio",
      ["-i", source_location, "-o", output_destination])

    case aglio_result do
      {_, 0} ->
        IO.puts "Generated API docs: #{output_destination}"

      {_, _} ->
        IO.puts :stderr, "Failed to generate API Docs for #{source_location}"
    end
  end
end

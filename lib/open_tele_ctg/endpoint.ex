defmodule OpenTeleCtg.Endpoint do
  use Phoenix.Endpoint, otp_app: :open_tele_ctg

  socket "/socket", OpenTeleCtg.UserSocket

  # Serve at "/" the static files from "priv/static" directory.
  #
  # You should set gzip to true if you are running phoenix.digest
  # when deploying your static files in production.
  plug Plug.Static,
    at: "/", from: :open_tele_ctg, gzip: false,
    only: ~w(css fonts images js favicon.ico robots.txt docs json_schemas)

  # Code reloading can be explicitly enabled under the
  # :code_reloader configuration of your endpoint.
  if code_reloading? do
    plug Phoenix.CodeReloader
  end

  plug Plug.RequestId
  plug Plug.Logger

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Poison

  plug OpenTeleCtg.Authenticate
  plug OpenTeleCtg.AuditLogger

  plug Plug.MethodOverride
  plug Plug.Head

  plug Plug.Session,
    store: :cookie,
    key: "_open_tele_ctg_key",
    signing_salt: "tDaNTQih"

  plug Corsica, allow_headers: ["accept", "authorization", "content-type", "origin"]
  plug OpenTeleCtg.Router
end

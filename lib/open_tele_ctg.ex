defmodule OpenTeleCtg do
  @moduledoc ~S"""
  Initializes the OpenTeleCtg application.
  """

  use Application
  alias OpenTeleCtg.Milou.{Discrete, Continuous}

  @doc ~S"""
  Starts the OpenTeleCtg application.

  Setup of:

  - database connections,
  - REST API endpoints,
  - JSON schema validation and caching,
  - Discrete CTG exporter, and
  - Continuous CTG exporter.
  """
  @spec start(any, any) :: Supervisor.on_start
  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      # Start the endpoint when the application starts
      supervisor(OpenTeleCtg.Endpoint, []),

      # Start the Ecto repository
      supervisor(OpenTeleCtg.Repo, []),
      supervisor(OpenTeleCtg.OpenTeleServer.Repo, []),

      # Start the JSON schema validator
      worker(OpenTeleCtg.JSONSchema.Validator, []),
      worker(OpenTeleCtg.JSONSchema.Cache, []),

      # Discrete CTG exporters
      worker(OpenTeleCtg.Milou.Discrete, []),
      supervisor(Discrete.Workers.CtgExportPool.Supervisor, []),

      # Continuous CTG exporters
      worker(Continuous.Registry, [Continuous.Registry]),
      supervisor(Continuous.Exporter.Supervisor, []),

      # Continuous CTG status registry
      worker(OpenTeleCtg.Milou.Continuous.StatusRegistry, [])
    ]

    opts = [strategy: :one_for_one, name: OpenTeleCtg.Supervisor]
    supervisor_reply = Supervisor.start_link(children, opts)

    # Pick up any CTGs that were in the process of being exported
    # when the server was stopped.
    # This has to happen after the supervisor has started the application
    milou_config = Application.get_env(:open_tele_ctg, OpenTeleCtg.Milou)
    discrete_exporter = Keyword.fetch!(milou_config, :discrete_exporter)
    discrete_exporter.pick_up_pending_exports()

    supervisor_reply
  end

  @doc ~S"""
  Tells Phoenix to update the endpoint configuration whenever
  the application is updated.
  """
  @spec config_change(any, any, any) :: :ok
  def config_change(changed, _new, removed) do
    OpenTeleCtg.Endpoint.config_change(changed, removed)
    :ok
  end

end
